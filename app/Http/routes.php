<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'web'], function () {
    Route::get('/','MainController@index');
    Route::get('/contacts','MainController@contacts');
    Route::get('/about-us','MainController@about_us');
    Route::get('/page/{id?}','MainController@page');

// catalog
    Route::get('/catalog','MainController@catalog');
    Route::post('/catalog','MainController@catalog_post');
//	Route::get('/recipe/{id?}','MainController@recipe');
    Route::get('/recipe/{name?}','MainController@recipe');

    Route::get('/advice','MainController@advice');
    Route::get('/advice/{id?}','MainController@advice');

    Route::get('/meat','MainController@meat');
    Route::get('/meat/{id?}','MainController@meat');

    Route::get('/actions','MainController@actions');
    Route::get('/actions/{id?}','MainController@actions');

    Route::get('/review','MainController@review');
    Route::get('/review/{id?}','MainController@review');
    Route::post('/review_add','MainController@review_add');
    //gallery
    Route::get('/gallery','MainController@gallery');
    Route::get('/gallery/{id?}','MainController@gallery');
    //rating
    Route::get('/rating','MainController@rating');
    // send mail
    Route::post('/send_mail','MainController@send_mail');

    //shop
    Route::get('/shop','ShopController@index');
    Route::get('/shop/categories/{category?}','ShopController@categories');
    Route::post('/shop/categories','ShopController@categories_post');
    Route::get('/shop/contacts','ShopController@contacts');
    Route::get('/shop/product/{id}','ShopController@product');

    // basket
    Route::get('/shop/basket','ShopController@basket');
    Route::get('/shop/ordering','ShopController@ordering');
    Route::post('/shop/add_to_cart','ShopController@add_to_cart');
    Route::post('/shop/set_time_delivery','ShopController@set_time_delivery');
    Route::post('/shop/send_ordering','ShopController@send_ordering');

    // api
    Route::get('/shop/getICML','ShopController@getICML');

    Route::get('/shop/about-us','ShopController@about_us');
    Route::get('/shop/actions/{id?}','ShopController@actions');
    Route::get('/shop/{name?}','ShopController@page');

    Route::get('/admin','Admin/MainController@index');
    Route::get('/{name?}','MainController@page');
    Route::get('/404}','MainController@404');
});