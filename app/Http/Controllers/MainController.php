<?php

namespace App\Http\Controllers;

use App\Classes\Base;
use App\Classes\DynamicModel;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
	/**
	 * ModuleController constructor.
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		parent::__construct();

		$this->dynamic = new DynamicModel();
		$this->request = $request->all();
		$this->requests = $request;
		$this->base = new Base($request);
	}

	/**
	 * @return mixed
	 */
	public function index()
	{
        $Mod =  $this->dynamic;
		$data['recipe'] = $Mod->t('recipe')
			->where(['recipe.active' => 1])

			->join('files', function($join)
			{
				$join->type = 'LEFT OUTER';
				$join->on('recipe.id', '=','files.id_album')
					->where('files.name_table', '=', 'recipe');
			})

			->select('recipe.*', 'files.file', 'files.crop')
			->groupBy('recipe.id')
			->orderBy('recipe.id', 'RAND)')
			->first();

		$where['advice.active'] = 1;
		$data['advice'] = $Mod->t('advice')
			->where($where)

			->join('files', function($join)
			{
				$join->type = 'LEFT OUTER';
				$join->on('advice.id', '=','files.id_album')
					->where('files.name_table', '=', 'advice');
			})

			->select('advice.*', 'files.file', 'files.crop')
			->groupBy('advice.id')
			->orderByRaw('RAND()')
			->get();


		return $this->base->view_s("site.main.index", $data);
	}

	/**
	 * контакты
	 * @return mixed
	 */
	public function contacts()
	{
		return $this->base->view_s("site.main.contacts"/*, $data*/);
	}

	/**
	 * о нас
	 * @return mixed
	 */
	public function about_us()
	{
		$Mod =  $this->dynamic;
		$data['about'] = $Mod->t('str')
			->where(['active' => 1, 'translation' => 'about-us',])
			->first();

		return $this->base->view_s("site.main.about_us", $data);
	}

	/**
	 * @param null $id
	 * @return mixed
	 */
	public function gallery($id = null)
	{
		$Mod =  $this->dynamic;
//        $data['album'] = $Mod->t('gallery')->where(['active' => 1])->get();

//        return $this->base->view_s("site.main.gallery", $data);


		if($id) {
			$data['album'] = $Mod->t('gallery')
				->where(['gallery.active' => 1])
				->where(['id' => $id])->get();

			if(!empty($data['album'])) {
				$data['meta_c']['title'] = $data['album'][0]->title;
				$data['meta_c']['description'] = $data['album'][0]->description;
				$data['meta_c']['keywords'] = $data['album'][0]->keywords;
			}

			$data['files'] = $Mod->t('files')
				->where(['files.active' => 1])
				->where(['id_album' => $id, 'name_table' => 'gallery'])->get();

			return $this->base->view_s("site.main.gallery_id", $data);
		} else {
			$data['album'] = $Mod->t('gallery')
				->where(['gallery.active' => 1])

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('gallery.id', '=','files.id_album')
						->where('files.name_table', '=', 'gallery')
						->where('files.main', '=', '1');
				})

				->select('gallery.*', 'files.file', 'files.crop')
				->groupBy('gallery.id')
				->orderBy('gallery.id', 'DESC')->get();

			return $this->base->view_s("site.main.gallery", $data);
		}
	}

	/**
	 * @param null $id
	 * @return mixed
	 */
	public function news($id = null)
	{
		$Mod =  $this->dynamic;

		if($id) {
			$data['news'] = $Mod->t('news')
				->where(['news.active' => 1])
				->where(['id' => $id])->get();
			$data['files'] = $Mod->t('files')
				->where(['files.active' => 1])
				->where(['id_album' => $id, 'name_table' => 'news'])->get();

			if(!empty($data['news'])) {
				$data['meta_c']['title'] = $data['news'][0]->title;
				$data['meta_c']['description'] = $data['news'][0]->description;
				$data['meta_c']['keywords'] = $data['news'][0]->keywords;
			}

			return $this->base->view_s("site.main.news_id", $data);
		} else {
			$data['news'] = $Mod->t('news')
				->where(['news.active' => 1])

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('news.id', '=','files.id_album')
						->where('files.name_table', '=', 'news');
				})

				->select('news.*', 'files.file', 'files.crop')
				->groupBy('news.id')
				->orderBy('news.id', 'DESC')->get();

			return $this->base->view_s("site.main.news", $data);
		}
	}

	/**
	 * страницы
	 * @param null $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function page($id = null)
	{
		$Mod =  $this->dynamic;

		if($id) {
			$data = [];
			$where[] = ['str.active', 1];


			if((int)$id == 0 || strlen($id) > 5) {
				$data['field'] = 'translation';
				$where[] = ['str.translation', $id];
			} else {
				$where['str.id'] = $id;
			}

			$data['page'] = $Mod->t('str')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('str.id', '=','files.id_album')
						->where('files.name_table', '=', 'str');
				})

				->select('str.*', 'files.file', 'files.crop')
				->get();

			if(empty($data['page'])) { return abort(404, 'Страница не существует'); }

			if(!empty($data['page'])) {
				$data['meta_c']['title'] = $data['page'][0]->title;
				$data['meta_c']['description'] = $data['page'][0]->description;
				$data['meta_c']['keywords'] = $data['page'][0]->keywords;
			}

			if(!empty($data['page'][0]))
			{
				$data['files'] = $Mod->t('files')
					->where(['files.active' => 1])
					->where(['id_album' => $data['page'][0]['id'], 'name_table' => 'str'])->get();
			} else {
				$data['files'] = [];
			}

			return $this->base->view_s("site.main.page_id", $data);
		} else {
			$data = [];
			$data['meta_c']['title'] = "Статьи";
			$menu= $Mod->t('menu')
				->select('id')
				->get();

			$notin = [];
			foreach ($menu as $v)
			{
				$notin[] = $v->id;
			}

			$data['page'] = $Mod->t('str')
				->where(['str.active' => 1])
				->whereNotIn('str.cat', $notin)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('str.id', '=','files.id_album')
						->where('files.name_table', '=', 'str');
				})

				->select('str.*', 'files.file', 'files.crop')
				->groupBy('str.id')
				->orderBy('str.id', 'DESC')->get();

			if((int)$id == 0) {
				$data['field'] = 'translation';
			}

			return $this->base->view_s("site.main.page", $data);
		}
	}

	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function catalog()
	{
		if(isset($this->request['page'])) {
			$data['page'] = $this->request['page'];
		} else {
			$data['page'] = 0;
		}

		return $this->base->view_s("site.general.catalog", $data);
	}

	public function catalog_post()
	{
		$request = $this->request;
		$Mod =  $this->dynamic;

		$where[] = ['recipe.active', 1];

		if($request['type_cooking'] != 0) {
			$where[] = ['recipe.type_cooking', $request['type_cooking']];
		}

		if($request['time_cooking'] != 0) {
			if($request['time_cooking'] == 1) {
				$where[] = ['recipe.time_cooking', 30];
			}

			if($request['time_cooking'] == 2) {
				$where[] = ['recipe.time_cooking', '>=', 30];
				$where[] = ['recipe.time_cooking', '<=', 60];
			}

			if($request['time_cooking'] == 3) {
				$where[] = ['recipe.time_cooking', '>=', 60];
			}
		}

		if($request['type_meat'] != 0) {
			$where[] = ['recipe.type_meat', $request['type_meat']];
		}

		if($request['simple_recipe'] != -1) {
			$where[] = ['recipe.simple_recipe', '<=', 40];
		}

		$group = 'id';

		if($request['filter_group'] == 10) {
			$group = 'id';
		}

		if($request['filter_group'] == 20) {
			$group = 'time_cooking';
		}

		if($request['filter_group'] == 30) {
			$group = 'rating';
		}

		$data['recipe'] = $Mod->t('recipe')
			->where($where)
			->where('recipe.text', 'like', '%' . trim($request['input_search']) . '%')

			->join('files', function($join)
			{
				$join->type = 'LEFT OUTER';
				$join->on('recipe.id', '=','files.id_album')
					->where('files.name_table', '=', 'recipe')

					->where('files.main', '=', 1);
//                    ->where('files.name_table', '=', 'recipe');
			})

			->select('recipe.*', 'files.file', 'files.crop')
			->groupBy('recipe.id')
			->orderBy('recipe.' . $group, 'DESC')
			->paginate(12);
//			->get();

		$data['pagination'] = ''.$data['recipe']->render();

		if(!empty($data['recipe'])) {
			$data['result'] = 'ok';
		} else {
			$data['result'] = 'error';
		}
		echo json_encode($data);
	}

	/**
	 * page recipe
	 * @param null $name
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function recipe($name = null)
	{
		if($name == null) abort(404, 'Страница не существует');
		(is_numeric($name)) ? $q_get = 'recipe.id' : $q_get = 'recipe.translation';

		$Mod =  $this->dynamic;

		$data['recipe'] = $Mod->t('recipe')
			->where(['recipe.active' => 1, $q_get => $name])

			->join('files', function($join)
			{
				$join->type = 'LEFT OUTER';
				$join->on('recipe.id', '=','files.id_album')
					->where('files.name_table', '=', 'recipe')

					->where('files.main', '=', 1);
			})

			->select('recipe.*', 'files.file', 'files.crop')
			->first();

		if(empty($data['recipe'])) abort(404, 'Страница не существует');

		$data['similar'] = $Mod->t('recipe')
			->where(['recipe.active' => 1, 'recipe.type_cooking' => $data['recipe']->type_cooking])
            ->whereNotIn($q_get, [$name])

			->join('files', function($join)
			{
				$join->type = 'LEFT OUTER';
				$join->on('recipe.id', '=','files.id_album')
					->where('files.name_table', '=', 'recipe')

					->where('files.main', '=', 1);
			})

			->select('recipe.*', 'files.file', 'files.crop')
			->limit(4)
			->get();

		if(!empty($data['recipe'])) {
			$data['meta_c']['title'] = $data['recipe']->title;
			$data['meta_c']['description'] = $data['recipe']->description;
			$data['meta_c']['keywords'] = $data['recipe']->keywords;
		}

		return $this->base->view_s("site.general.recipe", $data);
	}

	/**
	 * pages advice
	 * @param null $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function advice($id = null)
	{
		$Mod =  $this->dynamic;
		$request =  $this->requests;
		$id_advice = $request['id'];

		if($id && $id_advice) {
			$where[] = ['advice.active', 1];
			$where[] = ['advice.id', $id_advice];

			$data['advice'] = $Mod->t('advice')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('advice.id', '=','files.id_album')
						->where('files.name_table', '=', 'advice');
				})

				->select('advice.*', 'files.file', 'files.crop')
				->first();


			if(!empty($data['advice'][0]))
			{
				$data['files'] = $Mod->t('files')
					->where(['files.active' => 1])
					->where(['id_album' => $data['advice'][0]['id'], 'name_table' => 'advice'])->get();
			} else {
				$data['files'] = [];
			}

            $data['id_cat'] = $id;

			return $this->base->view_s("site.main.advice_id", $data);
		} else {
			$data = [];
			$data['meta_c']['title'] = "Советы";
			$menu= $Mod->t('menu')
				->select('id')
				->get();

			$where['advice.active'] = 1;

			if($id != 4) {
				$where['advice.cat'] = $id;

			}

			$data['advice'] = $Mod->t('advice')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('advice.id', '=','files.id_album')
						->where('files.name_table', '=', 'advice');
				})

				->select('advice.*', 'files.file', 'files.crop')
				->groupBy('advice.id')
				->orderBy('advice.id', 'DESC')
				->paginate(12);

			if((int)$id == 0) {
				$data['field'] = 'translation';
			}

			$data['page'] =  $request['page'];
			
			return $this->base->view_s("site.main.advice", $data);
		}
	}

	/**
	 * pages meat
	 * @param null $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function meat($id = null)
	{
		$Mod =  $this->dynamic;
		$request =  $this->requests;
		$id_meat =  $request['id'];

		if($id && $id_meat) {
			$where[] = ['meat.active', 1];
			$where[] = ['meat.id', $id_meat];

			$data['meat'] = $Mod->t('meat')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('meat.id', '=','files.id_album')
						->where('files.name_table', '=', 'meat');
				})

				->select('meat.*', 'files.file', 'files.crop')
				->first();


			if(!empty($data['meat'][0]))
			{
				$data['files'] = $Mod->t('files')
					->where(['files.active' => 1])
					->where(['id_album' => $data['meat'][0]['id'], 'name_table' => 'meat'])->get();
			} else {
				$data['files'] = [];
			}

			if(!empty($data['meat'])) {
				$data['meta_c']['title'] = $data['meat']->title;
				$data['meta_c']['description'] = $data['meat']->description;
				$data['meta_c']['keywords'] = $data['meat']->keywords;
			}

			return $this->base->view_s("site.main.meat_id", $data);
		} else {
			$data = [];
			$data['meta_c']['title'] = "Мясо";
			$menu= $Mod->t('menu')
				->select('id')
				->get();

			$where['meat.active'] = 1;

			if($id != 2) {
				$where['meat.cat'] = $id;

			}

			$data['meat'] = $Mod->t('meat')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('meat.id', '=','files.id_album')
						->where('files.name_table', '=', 'meat');
				})

				->select('meat.*', 'files.file', 'files.crop')
				->groupBy('meat.id')
				->orderBy('meat.id', 'DESC')
				->paginate(12);

			if($id == 2) {$idr = '';}

			if($id == 16) {$idr = 1;}
			if($id == 17) {$idr = 2;}
			if($id == 18) {$idr = 3;}
			if($id == 19) {$idr = 4;}
			if($id == 20) {$idr = 5;}

			$data['idsr'] = $idr;

			$whereR['recipe.active'] = 1;

			if($idr) {
				$whereR['recipe.type_meat'] = $idr;
			}

			$data['recipe'] = $Mod->t('recipe')
				->where($whereR)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('recipe.id', '=','files.id_album')
						->where('files.name_table', '=', 'recipe');
				})

				->select('recipe.*', 'files.file', 'files.crop')
				->groupBy('recipe.id')
				->orderBy('recipe.id', 'DESC')
				->paginate(4);

			if((int)$id == 0) {
				$data['field'] = 'translation';
			}

			if($id == 2) {$ids = '';}

			if($id == 16) {$ids = 23;}
			if($id == 17) {$ids = 24;}
			if($id == 18) {$ids = 25;}
			if($id == 19) {$ids = 26;}
			if($id == 20) {$ids = 27;}

			$data['ids'] = $ids;

			$whereS['advice.active'] = 1;

			if($ids) {
				$whereS['advice.cat'] = $ids;
			}

			$data['advice'] = $Mod->t('advice')
				->where($whereS)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('advice.id', '=','files.id_album')
						->where('files.name_table', '=', 'advice');
				})

				->select('advice.*', 'files.file', 'files.crop')
				->groupBy('advice.id')
				->orderBy('advice.id', 'DESC')
				->paginate(4);

			if((int)$id == 0) {
				$data['field'] = 'translation';
			}

			$data['page'] =  $request['page'];

			return $this->base->view_s("site.main.meat", $data);
		}
	}

	/**
	 * pages actions
	 * @param null $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function actions($id = null)
	{
		$Mod =  $this->dynamic;
		$request =  $this->requests;
		$id_actions =  $request['id'];

		if($id || $id_actions) {
			$where[] = ['actions.active', 1];
			$where[] = ['actions.id', $id_actions];

			$data['actions'] = $Mod->t('actions')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('actions.id', '=','files.id_album')
						->where('files.name_table', '=', 'actions');
				})

				->select('actions.*', 'files.file', 'files.crop')
				->first();


			if(!empty($data['actions'][0]))
			{
				$data['files'] = $Mod->t('files')
					->where(['files.active' => 1])
					->where(['id_album' => $data['actions'][0]['id'], 'name_table' => 'actions'])->get();
			} else {
				$data['files'] = [];
			}

			if(empty($data['actions'])) { return abort(404, 'Страница не существует'); }

			if(!empty($data['actions'])) {
				$data['meta_c']['title'] = $data['actions']['title'];
				$data['meta_c']['description'] = $data['actions']['description'];
				$data['meta_c']['keywords'] = $data['actions']['keywords'];
			}

			return $this->base->view_s("site.main.actions_id", $data);
		} else {
			$data = [];
			$data['meta_c']['title'] = "Акции";
			$menu= $Mod->t('menu')
				->select('id')
				->get();

			$where['actions.active'] = 1;

			if($id != 4) {
//				$where['actions.cat'] = $id;

			}

			$data['actions'] = $Mod->t('actions')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('actions.id', '=','files.id_album')
						->where('files.name_table', '=', 'actions');
				})

				->select('actions.*', 'files.file', 'files.crop')
				->groupBy('actions.id')
				->orderBy('actions.id', 'DESC')
				->paginate(12);

			if((int)$id == 0) {
				$data['field'] = 'translation';
			}

			$data['page'] =  $request['page'];

			return $this->base->view_s("site.main.actions", $data);
		}
	}

	/**
	 * pages review
	 * @param null $id
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function review($id = null)
	{
		$Mod =  $this->dynamic;
		$request =  $this->requests;
		$id_review =  $request['id'];

		if($id || $id_review) {
			$where[] = ['review.active', 1];
			$where[] = ['review.id', $id_review];

			$data['review'] = $Mod->t('review')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('review.id', '=','files.id_album')
						->where('files.name_table', '=', 'review');
				})

				->select('review.*', 'files.file', 'files.crop')
				->first();


			if(!empty($data['review'][0]))
			{
				$data['files'] = $Mod->t('files')
					->where(['files.active' => 1])
					->where(['id_album' => $data['review'][0]['id'], 'name_table' => 'review'])->get();
			} else {
				$data['files'] = [];
			}

			return $this->base->view_s("site.main.review_id", $data);
		} else {
			$data = [];
			$data['meta_c']['title'] = "Отзывы";
			$menu= $Mod->t('menu')
				->select('id')
				->get();

			$where['review.active'] = 1;

			if($id != 4) {
//				$where['review.cat'] = $id;

			}

			$data['review'] = $Mod->t('review')
				->where($where)

				->join('files', function($join)
				{
					$join->type = 'LEFT OUTER';
					$join->on('review.id', '=','files.id_album')
						->where('files.name_table', '=', 'review');
				})

				->select('review.*', 'files.file', 'files.crop')
				->groupBy('review.id')
				->orderBy('review.id', 'DESC')
				->paginate(12);

			if((int)$id == 0) {
				$data['field'] = 'translation';
			}

			$data['page'] =  $request['page'];

			return $this->base->view_s("site.main.review", $data);
		}
	}

	/**
	 * function add review
	 */
	public function review_add()
	{
		$Mod =  $this->dynamic;
		$request =  $this->requests;

		if($request['text'] && $request['name'])
		{
			$rev['text'] = $request['text'];
			$rev['name'] = $request['name'];
			$rev['title'] = $request['name'];
			$rev['email'] = $request['email'];
			$rev['phone'] = $request['phone'];
			$rev['active'] = 0;
			$rev['updated_at'] = Carbon::now();
			$rev['created_at'] = Carbon::now();

			$id = $Mod->t('review')->insertGetId($rev);

			$param = $Mod->t('params')
				->select('params.*', 'little_description as key')
				->where('name', 'email')
				->first();

			Mail::send('emails.send_review', $rev, function ($m) use ($param) {
				$m->from('robot@poundmeat.ru', 'На сайте "Фунтмяса" добавлен новый отзыв');
				$m->to($param->key, 'Администратор')->subject('На сайте "Фунтмяса" добавлен новый отзыв');
			});

			$req['result'] = 'ok';
			$req['id'] = $id;
		} else {
			$req['result'] = 'error';
		}

		echo json_encode($req);
	}

	/**
	 * rating
	 */
	public function rating()
	{
		$request =  $this->requests;
		$id_recipe =  $request['id_recipe'];
		$rating =  $request['rating'];
		$ip =  $request->ip();

		if($id_recipe) {
			$Mod =  $this->dynamic;
			$vote = $Mod->t('rating')
				->where(['ip' => $ip, 'id_recipe' => $id_recipe])
				->get();

			$count = count($vote);

			if(!$count)
			{
				$count = 1;
				$sum_rating = 10;
			} else {
				$sum_rating = 0;
			}

			foreach ($vote as $v)
			{
				$sum_rating += $v->rating;
			}

			$total = round(($sum_rating + $rating)/($count + 1));
			$total_des = round(($sum_rating + $rating)/($count + 1), 1);

			$data = [];

			if(empty($vote[0]))
			{
				$data['ip'] = $ip;
				$data['id_recipe'] = $id_recipe;
				$data['rating'] = $rating;

				$insertGetId = $Mod->t('rating')->insertGetId($data);

				$recipe = $Mod->t('recipe')
					->where(['id' => $id_recipe])
					->first();

				$recipe->rating = $total_des;

				$recipe->save();

				$req['rating'] = $total;
				$req['rating_des'] = $total_des;
				$req['result'] = 'ok';
			} else {
				$req['rating'] = 'error';
				$req['rating_des'] = $total_des;
				$req['result'] = 'ok';
			}
		} else {
			$req['result'] = 'error';
		}

		echo json_encode($req);
	}

	public function send_mail()
	{
		$request =  $this->requests;
		$data['name'] = $request['name'];
		$data['phone'] = $request['phone'];
		$data['type'] = $request['type'];
		$data['text'] = $request['text'];

		if(trim($data['name']) && trim($data['phone']))
		{
			$Mod =  $this->dynamic;

			$param = $Mod->t('params')
				->select('params.*', 'little_description as key')
				->where('name', 'email')
				->first();

			Mail::send('emails.send_callback', $data, function ($m) use ($param) {
				$m->from('robot@poundmeat.ru', 'Заказ обратного звонка с сайта poundmeat.ru');
				$m->to($param->key, 'Администратор')->subject('Заказ обратного звонка с сайта poundmeat.ru');
			});

			$req['result'] = 'ok';
		} else {
			$req['result'] = 'error';
		}

		echo json_encode($req);
	}
}
