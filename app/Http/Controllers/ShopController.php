<?php

namespace App\Http\Controllers;

use App\Classes\Base;
use App\Classes\DynamicModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ShopController extends Controller
{
    /**
     * Type measure
     * @var array
     */
    protected $type_measure;

    /**
     * @var DynamicModel
     */
    protected $dynamic;

    /**
     * Request
     * @var array
     */
    protected $request;

    /**
     * @var Request
     */
    protected $requests;

    /**
     * @var Base
     */
    protected $base;

    /**
     * @var \RetailCrm\ApiClient
     */
    protected $retail_client;

    /**
     * ModuleController constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->dynamic      = new DynamicModel();
        $this->request      = $request->all();
        $this->requests     = $request;
        $this->base         = new Base($request);
        $this->type_measure = ['шт', 'кг', 'л', 'шт', 'шт'];

        $this->retail_client = new \RetailCrm\ApiClient(
            env('RETAIL_URL'),
            env('RETAIL_KEY'),
            \RetailCrm\ApiClient::V5
        );
    }

    /**
     * @param $arr
     * @param $val
     * @param string $name
     * @return array
     */
    function search_menu($arr, $val, $name = '') {
        $search = [];

        foreach($arr as $v)
            if($v[$val] == $name)
                $search[] = $v;

        return $search;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $Mod                       =  $this->dynamic;
        $where                     = [];
        $data                      = [];
        $where['menu_shop.active'] = 1;

        $menu_shop = $this->dynamic->t('menu_shop')
            ->where($where)

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('menu_shop.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'menu_shop');
            })

            ->select('menu_shop.*', 'files.file', 'files.crop')
            ->get()
            ->toArray();

        $where = [];
        $where['actions.active'] = 1;
        $data['actions'] = $Mod->t('actions')
            ->where($where)

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('actions.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'actions');
            })

            ->select('actions.*', 'files.file', 'files.crop')
            ->groupBy('actions.id')
            ->orderByRaw('RAND()')
            ->orderBy('actions.id', 'DESC')
            ->limit(5)
            ->get()
            ->toArray();

        $parent                  = $this->search_menu($menu_shop, 'translation', '/shop/categories');
        $parent_id               = isset($parent[0]['id']) ? $parent[0]['id'] : 0;
        $data['shop_categories'] = $this->search_menu($menu_shop, 'cat', $parent_id);

        return $this->base->view_s("shop.main.index", $data, 'menu_shop');
    }

    /**
     * страницы
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function page($id = null)
    {
        if($id) {
            $data = [];
            $where[] = ['str.active', 1];

            if((int)$id == 0 || strlen($id) > 5) {
                $data['field'] = 'translation';
                $where[] = ['str.translation', $id];
            } else
                $where['str.id'] = $id;

            $data['page'] = $this->dynamic->t('str')
                ->where($where)

                ->join('files', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('str.id', '=','files.id_album')
                        ->where('files.name_table', '=', 'str');
                })

                ->select('str.*', 'files.file', 'files.crop')
                ->get();

            if(empty($data['page'])) return abort(404, 'Страница не существует');

            if(!empty($data['page'][0])) {
                $data['meta_c']['title'] = $data['page'][0]->title;
                $data['meta_c']['description'] = $data['page'][0]->description;
                $data['meta_c']['keywords'] = $data['page'][0]->keywords;
            }

            if(!empty($data['page'][0]))
                $data['files'] = $this->dynamic->t('files')
                    ->where(['files.active' => 1])
                    ->where(['id_album' => $data['page'][0]['id'], 'name_table' => 'str'])->get();
            else
                $data['files'] = [];

            return $this->base->view_s("shop.main.page_id", $data, 'menu_shop');
        } else {
            $data = [];
            $data['meta_c']['title'] = "Статьи";
            $menu= $this->dynamic->t('menu')
                ->select('id')
                ->get();

            $notin = [];
            foreach ($menu as $v)
                $notin[] = $v->id;

            $data['page'] = $this->dynamic->t('str')
                ->where(['str.active' => 1])
                ->whereNotIn('str.cat', $notin)

                ->join('files', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('str.id', '=','files.id_album')
                        ->where('files.name_table', '=', 'str');
                })

                ->select('str.*', 'files.file', 'files.crop')
                ->groupBy('str.id')
                ->orderBy('str.id', 'DESC')->get();

            if((int)$id == 0)
                $data['field'] = 'translation';

            return $this->base->view_s("shop.main.page", $data);
        }
    }

    /**
     * about-us
     * @param string $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about_us($id = 'about-us')
    {
        $data    = [];
        $where[] = ['str.active', 1];

        if((int)$id == 0 || strlen($id) > 5) {
            $data['field'] = 'translation';
            $where[] = ['str.translation', $id];
        } else
            $where['str.id'] = $id;

        $data['page'] = $this->dynamic->t('str')
            ->where($where)

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('str.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'str');
            })

            ->select('str.*', 'files.file', 'files.crop')
            ->get();

        if(empty($data['page'])) return abort(404, 'Страница не существует');

        if(!empty($data['page'][0])) {
            $data['meta_c']['title'] = $data['page'][0]->title;
            $data['meta_c']['description'] = $data['page'][0]->description;
            $data['meta_c']['keywords'] = $data['page'][0]->keywords;
        }

        if(!empty($data['page'][0])) {
            $data['files'] = $this->dynamic->t('files')
                ->where(['files.active' => 1])
                ->where(['id_album' => $data['page'][0]['id'], 'name_table' => 'str'])->get();
        } else
            $data['files'] = [];

        return $this->base->view_s("shop.main.about_us", $data, 'menu_shop');
    }

    /**
     * товары
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function product($id = null)
    {
        $data = [];
        $where[] = ['product.active', 1];

        if((int)$id == 0 || strlen($id) > 5) {
            $data['field'] = 'translation';
            $where[] = ['product.translation', $id];
        } else
            $where['product.id'] = $id;

        $data['product'] = $this->dynamic->t('product')
            ->where($where)

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'product')
                    ->where('files.main', '=', 1);
            })

            ->select('product.*', 'files.file', 'files.crop')
            ->groupBy('product.id')
            ->first();

        $data['products_sell_out'] = $this->_sell_out($id);
        if(empty($data['product'])) return abort(404, 'Страница не существует');

        if(!empty($data['product'])) {
            $data['meta_c']['title'] = $data['product']['title'];
            $data['meta_c']['description'] = $data['product']['description'];
            $data['meta_c']['keywords'] = $data['product']['keywords'];
        }

        if(!empty($data['product']))
            $data['files'] = $this->dynamic->t('files')
                ->where(['files.active' => 1])
                ->where(['id_album' => $data['product']['id'], 'name_table' => 'product'])->get();
        else
            $data['files'] = [];

        return $this->base->view_s("shop.main.product_id", $data, 'menu_shop');
    }

    /**
     * корзина
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function basket()
    {
        $where[]   = ['product.active', 1];
        $group     = 'id';
        $count_box = 24;
        $cart      = array_values($this->requests->session()->get('cart') ?? []);

        for($i = 0; count($cart ?? []) > $i; $i++)
            $cart_id[] = $cart[$i]['id'] ?? 0;

        $data['product'] = $this->dynamic->t('product')
            ->where($where)
            ->whereIn('product.id', $cart_id ?? [])
            ->where('product.text', 'like', '%' . trim($request['input_search'] ?? '') . '%')

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'product')
                    ->where('files.main', '=', 1);
            })

            ->select('product.*', 'files.file', 'files.crop')
            ->groupBy('product.id')
            ->orderBy('product.' . $group, 'DESC')
            ->paginate($count_box);

        foreach($cart as $v)
            $data['cart'][$v['id']] = $v;

        $data['products_sell_out'] = $this->_sell_out($cart_id ?? []);
        $data['type_measure']      = $this->type_measure;

        return $this->base->view_s("shop.main.basket", $data, 'menu_shop');
    }

    /**
     * добавление время и адреса доставки
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function set_time_delivery()
    {
        $address            = $this->request['address'];
        $time_current       = $this->request['time_current'];
        $time_current_month = $this->request['time_current_month'];
        $time_current_day   = $this->request['time_current_day'];

        $this->requests->session()->put('address', $address);
        $this->requests->session()->put('time_current', $time_current);
        $this->requests->session()->put('time_current_month', $time_current_month);
        $this->requests->session()->put('time_current_day', $time_current_day);

        $data['address']            = $this->requests->session()->get('address');
        $data['time_current']       = $this->requests->session()->get('time_current');
        $data['time_current_month'] = $this->requests->session()->get('time_current_month');
        $data['time_current_day']   = $this->requests->session()->get('time_current_day');

        $t = explode('-', $time_current);

        $data['time_current_all'] = count($t) == 3
            ? (int) $time_current_day . ' '. $time_current_month . ', ' . 'с ' . $t[0] . ' до ' . $t[1]
            : 'время не выбрано';

        $data['result']             = 'ok';

        return json_encode($data);
    }

    public function send_ordering()
    {
        $cart            = json_encode(array_values($this->requests->session()->get('cart') ?? []));
        $address         = $this->request['address'];
        $apartment       = $this->request['apartment'];
        $city            = $this->request['city'];
        $comment_1       = $this->request['comment_1'];
        $comment_2       = $this->request['comment_2'];
        $delivery_time_1 = $this->request['delivery_time_1'];
        $district        = $this->request['district'] ?? '';
        $entrance_floor  = $this->request['entrance_floor'];
        $email           = $this->request['email'];
        $full_name       = $this->request['full_name'];
        $payment_method  = $this->request['payment_method'];
        $phone           = $this->request['phone'];
        $private_house   = $this->request['private_house'];
        $total_cost      = 0;

        $cart_to_id = [];
        $where[]   = ['product.active', 1];
        $group = 'id';
        $count_box = 24;
        $cart_0 = array_values($this->requests->session()->get('cart') ?? []);

        for($i = 0; count($cart_0 ?? []) > $i; $i++) {
            $cart_id[] = $cart_0[$i]['id'] ?? 0;
            $cart_to_id[$cart_0[$i]['id']] = $cart_0[$i];
        }

        $products = $this->dynamic->t('product')
            ->where($where)
            ->whereIn('product.id', $cart_id ?? [])

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'product')
                    ->where('files.main', '=', 1);
            })

            ->join('menu', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.cat', '=','menu.id');
            })

            ->select('product.*', 'files.file', 'files.crop', 'menu.name as cat_parent')
            ->groupBy('product.id')
            ->orderBy('product.' . $group, 'DESC')
            ->get()
            ->toArray();

        $items = [];

        foreach($products as $val) {
            $total_cost += $initialPrice = ($val['price_money'] - ($val['price_money'] / 100 * $val['district']))
            / 1000 * $cart_to_id[$val['id']]['weights'];

            $items[] = [
                'offer' => ['xmlId' => $val['id']],

                // зависит от типа, если кг то это считаем что покупают в порцииях(порция 100грамм) если шт то делим на 1000
                'quantity' => ($val['type_measure'] == 1 || $val['type_measure'] == 2)
                    ? ($cart_to_id[$val['id']]['weights'] / 100)
                    : $cart_to_id[$val['id']]['weights']  / 1000,

                'properties' => [[
                    'code' => 'weight',
                    'name' => ['Штука', 'Килограмм', 'Литр', 'Штука', 'Штука'][$val['type_measure']] ?? '',
                    'value' => ([1, 1.0, 1.0, 1, 1][$val['type_measure']] ?? '') .
                        ' ' .
                        (['шт', 'кг', 'л', 'шт', 'шт'][$val['type_measure']] ?? ''),
                ]],

                // если измерение не в штуках то делим на 10 что бы получить цену за 100грамм
                'initialPrice' => ($val['type_measure'] == 1 || $val['type_measure'] == 2)
                    ? ($val['price_money'] - ($val['price_money'] / 100 * $val['district'])) / 10
                    : ($val['price_money'] - ($val['price_money'] / 100 * $val['district']))
                ,
            ];
        }

        $applications = $this->dynamic->t('applications');

        $applications->active = 1;
        $applications->status_app = 0;
        $applications->address = $address;
        $applications->apartment = $apartment;
        $applications->comment_1 = $comment_1;
        $applications->comment_1 = $comment_2;
        $applications->delivery_time_1 = $delivery_time_1;
        $applications->district = $district;
        $applications->entrance_floor = $entrance_floor;
        $applications->email = $email;
        $applications->full_name = $full_name;
        $applications->payment_method = $payment_method;
        $applications->phone = $phone;
        $applications->private_house = $private_house;
        $applications->total_cost = $total_cost;

        $applications->save();

        /**/
        $response = $this->retail_client->request->ordersCreate(array(
            'externalId' => null,
            'firstName' => explode(' ', $full_name)[0] ?? '',
            'lastName' => explode(' ', $full_name)[1] ?? '',
            'patronymic' => explode(' ', $full_name)[2] ?? '',
            'email' => $email,
            'phone' => $phone,
            'customerComment' => $comment_1 . "\r\n" . $comment_2,

//            'payments' => [
//                ['type' => $payment_method  == 'Наличный расчёт' ? 'cash' : 'bank-card-courier'],
//            ],


            'items' => $items,

            'delivery' => [
                'address' => $city . ', ' .
                    $address . '. ' .
                    'Подъезд/этаж:' . $entrance_floor . '. ' .
                    'Квартира или офис:' . $apartment,

//                'code' => 'russian-post',
                'time' => ['custom' => $delivery_time_1]
            ]
        ));
        /**/

        $param = $this->dynamic
            ->t('params')
            ->select('params.*', 'little_description as key')
            ->where('name', 'email_alerts')
            ->first();

        $params = [
            'full_name' => $full_name,
            'phone' => $phone,
            'address' => $address,
            'email' => $email,
        ];

        /* send mail to admin */
        Mail::send('emails.shop.notification', $params, function($m) use($param) {
            $m->from('robot@poundmeat.ru', 'Новый заказ на сайте poundmeat.ru');
            $m->to($param->key, 'Администратор')->subject('Новый заказ на сайте poundmeat.ru');
        });

        /* send mail to customer */
        Mail::send('emails.shop.user_notification', $params, function($m) use($param, $params) {
            $m->from('robot@poundmeat.ru', 'Оповещение сайта poundmeat.ru');
            $m->to($param->key, $param['full_name'])->subject('Оповещение сайта poundmeat.ru');
        });

        // save applications_time
        $applications_time               = [];
        $applications_time['parent_id']  = $applications->id;
        $applications_time['status']     = 0;
        $applications_time['updated_at'] = Carbon::now();
        $applications_time['created_at'] = Carbon::now();
        $applications_time['date_time']  = Carbon::now();

        $this->dynamic->t('applications_time')->insertGetId($applications_time);

        foreach(json_decode($cart, true) as $value) {
            // save applications_time
            $applications_ordering               = [];
            $applications_ordering['applications_id'] = $applications->id;
            $applications_ordering['product_id']      = $value['id'];
            $applications_ordering['weights']         = $value['weights'];
            $applications_ordering['updated_at']      = Carbon::now();
            $applications_ordering['created_at']      = Carbon::now();
            $applications_ordering['user_id']         = 0;

            $this->dynamic->t('applications_ordering')->insertGetId($applications_ordering);
        }

//        $this->requests->session()->flush('cart');
        $data['result'] = 'ok';
        $data['$applications_end'] = $applications;
        $data['response_orders'] = $response;
        $data['$items'] = $items;

        return json_encode($data);
    }

    /**
     * добавление/удаление в коризу
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add_to_cart()
    {
        $idd      = false;
        $id       = $this->request['id'];
        $get_data = $this->request['get_data'];
        $type     = $this->request['type'];
        $weights  = $this->request['weights'];
        $cart     = array_values($this->requests->session()->get('cart') ?? []);

        if($type === 'add') {
            for($i = 0; count($cart) > $i; $i++)
                if($cart[$i]['id'] == $id) {
                    $idd = true;
                    break;
                }

            if(!$idd)
                $this->requests->session()->put('cart',
                    array_merge($cart, [$id => ['id' => $id, 'weights' => $weights]]));
        }

        if($type === 'remove') {
            for($i = 0; count($cart) > $i; $i++)
                if($cart[$i]['id'] == $id) {
                    unset($cart[$i]);
                    break;
                }

            $this->requests->session()->flush('cart');
            $this->requests->session()->put('cart', $cart);
        }

        if($type === 'update') {
            for($i = 0; count($cart) > $i; $i++)
                if($cart[$i]['id'] == $id) {
                    $cart[$i]['weights'] = $weights;
                    break;
                }

            $this->requests->session()->put('cart', $cart);
        }

        if($get_data) {
            $where[]   = ['product.active', 1];
            $group = 'id';
            $count_box = 24;
            $cart_0 = array_values($this->requests->session()->get('cart') ?? []);

            for($i = 0; count($cart_0 ?? []) > $i; $i++)
                $cart_id[] = $cart_0[$i]['id'] ?? 0;

            $data['product'] = $this->dynamic->t('product')
                ->where($where)
                ->whereIn('product.id', $cart_id ?? [])
                ->where('product.text', 'like', '%' . trim($request['input_search'] ?? '') . '%')

                ->join('files', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('product.id', '=','files.id_album')
                        ->where('files.name_table', '=', 'product')
                        ->where('files.main', '=', 1);
                })

                ->join('menu', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('product.cat', '=','menu.id');
                })

                ->select('product.*', 'files.file', 'files.crop', 'menu.name as cat_parent')
                ->groupBy('product.id')
                ->orderBy('product.' . $group, 'DESC')
                ->paginate($count_box);
        }

        foreach($this->requests->session()->get('cart') ?? [] as $v)
            $data['cart'][$v['id']] = $v;

        $data['result'] = 'ok';

        return json_encode($data);
    }

    /**
     * Create getICML from https://poundmeat.retailcrm.ru/
     */
    public function getICML()
    {
        $products = $this->dynamic->t('product')
            ->select('product.*', 'files.file', 'files.crop')
            ->where('product.active', '=',1)

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'product')
                    ->where('files.main', '=', 1);
            })

            ->get()
            ->toArray();

        $cat = $this->dynamic->t('menu_shop')->where(['active' => 1])->orderBy('order', 'ASC')->get()->toArray();

        $dom = new \domDocument("1.0", "utf-8");
        $root = $dom->createElement("yml_catalog");
        $root->setAttribute("date", date('Y-m-d h:i:s'));
        $dom->appendChild($root);

        $shop = $dom->createElement("shop");
        $root->appendChild($shop);

        $name = $dom->createElement("name", "Фунт мяса");
        $company = $dom->createElement("company", "Фунт мяса");

        $shop->appendChild($name);
        $shop->appendChild($company);

        // add categories
        $categories = $dom->createElement("categories");
        $shop->appendChild($categories);

        // add offers
        $offers = $dom->createElement("offers");
        $shop->appendChild($offers);

        foreach($cat as $key => $v) {
            $c = $dom->createElement("category", $v['name']);
            $c->setAttribute("id", $v['id']);

            if($v['cat'])
                $c->setAttribute("parentId", $v['cat']);

            $categories->appendChild($c);
        }

        foreach($products as $key => $product) {
            $offer = $dom->createElement("offer");
            $offer->setAttribute("id", $product['id']);
            $offer->setAttribute("productId", $product['id']);
            $offer->setAttribute("quantity", 1000);

            $offer->appendChild($dom->createElement("price", $product['price_money']));

            $offer->appendChild($dom->createElement(
                "purchasePrice",
                $product['price_money'] - ($product['price_money']/100 * $product['discount'])
            ));

            $offer->appendChild($dom->createElement("categoryId", $product['cat']));

            $offer->appendChild($dom->createElement("picture", $product['file']
                ? ($product['crop']
                    ? 'http://' . $_SERVER['HTTP_HOST'] . '/images/files/big/' . $product['crop']
                    : 'http://' . $_SERVER['HTTP_HOST'] . '/images/files/big/' . $product['file'])
                : '')
            );

            $offer->appendChild($dom->createElement("name", $product['name']));
            $offer->appendChild($dom->createElement("productName", $product['name']));
            $offer->appendChild($dom->createElement("xmlId", $product['id']));

            $art = $dom->createElement("param", '#' . $product['id']);
            $art->setAttribute("name", 'Артикул');
            $offer->appendChild($art);

            $wg = $dom->createElement(
                "param",
                ([1, 1.0, 1.0, 1, 1][$product['type_measure']] ?? '') .
                ' ' .
                (['шт', 'кг', 'л', 'шт', 'шт'][$product['type_measure']] ?? '')
            );

            $wg->setAttribute("name", 'Вес');
            $wg->setAttribute("code", 'weight');
            $offer->appendChild($wg);

            $unit = $dom->createElement("unit", $product['id']);
            $unit->setAttribute("code", 'pcs');
            $unit->setAttribute("name", ['Штука', 'Килограмм', 'Литр', 'Штука', 'Штука'][$product['type_measure']] ?? '');
            $unit->setAttribute("sym",  ['шт', 'кг', 'л', 'шт', 'шт'][$product['type_measure']] ?? '' . '.');
            $offer->appendChild($unit);

            $offer->appendChild($dom->createElement("vatRate", 0));
            $offer->appendChild($dom->createElement("weight", [1, 1.0, 1.0, 1, 1][$product['type_measure']] ?? ''));


            $offers->appendChild($offer);
        }

        header('Content-type: text/xml');
        echo $dom->saveXML();
    }

    /**
     * оформление товара
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ordering()
    {
        $data['city'] = $this->dynamic->t('params_city')->where(['active' => 1])->get()->toArray();
        $data['area'] = $this->dynamic->t('params_area')->where(['active' => 1])->get()->toArray();
        return $this->base->view_s("shop.main.ordering", $data, 'menu_shop');
    }

    /**
     * @param string $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories($category = '-1')
    {
        if(isset($this->request['page']))
            $data['page'] = $this->request['page'];
        else
            $data['page'] = 0;

        $data['category'] = $category;
        $where['menu_shop.active'] = 1;

        $menu_shop = $this->dynamic->t('menu_shop')
            ->where($where)

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('menu_shop.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'menu_shop');
            })

            ->select('menu_shop.*', 'files.file', 'files.crop')
            ->get()
            ->toArray();

        $data['current_category'] = $this->search_menu($menu_shop, 'id', $category);
        $data['parent_category'] = isset($data['current_category'][0]['cat'])
            ? $this->search_menu($menu_shop, 'id', $data['current_category'][0]['cat'])
            : [];

        if(!empty($data['current_category'][0])) {
            $data['meta_c']['title']       = 'Категория - ' . $data['current_category'][0]['title'] . ' - Онлай магазин "Фунт мяса"';
            $data['meta_c']['name']        = $data['current_category'][0]['name'];
            $data['meta_c']['description'] = $data['current_category'][0]['description'];
            $data['meta_c']['keywords']    = $data['current_category'][0]['keywords'];
        }

        $cat_c = current($this->base->get_cat('menu_shop', ['active' => 1, 'cat' => $data['current_category'][0]['cat'] ?? -1]));
        $cat_i = current($this->base->get_cat('menu_shop', ['active' => 1, 'cat' => $data['current_category'][0]['id'] ?? -1]));
        $data['current_categories'] = !empty($cat_i) ? $cat_i : $cat_c;

        return $this->base->view_s("shop.main.categories", $data, 'menu_shop');
    }

    public function categories_post()
    {
        $request   = $this->request;
        $where[]   = ['product.active', 1];
        $group     = !empty($request['order_by']) ? $request['order_by'] : 'id';
        $count_box = $request['count_box'] ?? 6;

        if($request['filter_group'] == 10)
            $group = 'id';

        if($request['filter_group'] == 20)
            $group = 'time_cooking';

        if($request['filter_group'] == 30)
            $group = 'rating';

        if((int) $request['actions'] === 1)
            $where[] = ['product.popular_product', 1];

        if((int) $request['seasonal_goods'] === 1)
            $where[] = ['product.seasonal_goods', 1];

        /* get all subcategories */
        $menu_id = $this->dynamic->t('menu_shop')
            ->where(['active' => 1, 'cat' => $request['category'] == -1 ? 15 : $request['category']])
            ->select('id')
            ->get()
            ->toArray();

        $menu_id_arr = [];
        foreach($menu_id as $val)
            $menu_id_arr[] = $val['id'];
        /* get all subcategories */

        if($request['category'] == 11) {
            $where_in = [];
            $where[] = ['seasonal_goods', 1];
        } else
            $where_in = array_merge([$request['category']], $menu_id_arr);

        $tmp_query = $this->dynamic->t('product')
            ->where($where);

        if(!empty($where_in))
            $tmp_query->whereIN('product.cat', $where_in);

        $data['product'] = $tmp_query
            ->where('product.text', 'like', '%' . trim($request['input_search'] ?? '') . '%')
            ->orWhere('product.name', 'like', '%' . trim($request['input_search'] ?? '') . '%')

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'product')
                    ->where('files.main', '=', 1);
            })

            ->select('product.*', 'files.file', 'files.crop')
            ->groupBy('product.id')
            ->orderBy('product.' . $group, 'DESC')
            ->paginate($count_box);

        $data['pagination'] = ''.$data['product']->render();
        if(!empty($data['product'])) $data['result'] = 'ok'; else $data['result'] = 'error';

        echo json_encode($data);
    }

    /**
     * pages actions
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function actions($id = null)
    {
        $Mod        =  $this->dynamic;
        $request    =  $this->requests;
        $id_actions =  $request['id'] ? $request['id'] : $id;

        if($id || $id_actions) {
            $where[] = ['actions.active', 1];
            $where[] = ['actions.id', $id_actions];

            $data['actions'] = $Mod->t('actions')
                ->where($where)

                ->join('files', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('actions.id', '=','files.id_album')
                        ->where('files.name_table', '=', 'actions');
                })

                ->select('actions.*', 'files.file', 'files.crop')
                ->first();

          //dd($id_actions);
        //  dd($data);

            if(!empty($data['actions'])) {
                $data['files'] = $Mod->t('files')
                    ->where(['files.active' => 1])
                    ->where(['id_album' => $data['actions'][0]['id'], 'name_table' => 'actions'])->get();
            } else
                $data['files'] = [];

            if(empty($data['actions'])) return abort(404, 'Страница не существует');

            if(!empty($data['actions'])) {
                $data['meta_c']['title'] = $data['actions']['title'];
                $data['meta_c']['description'] = $data['actions']['description'];
                $data['meta_c']['keywords'] = $data['actions']['keywords'];
            }

            return $this->base->view_s("shop.main.actions_id", $data, 'menu_shop');
        } else {
            $data = [];
            $data['meta_c']['title'] = "Акции";
            $menu= $Mod->t('menu')
                ->select('id')
                ->get();

            $where['actions.active'] = 1;

            $data['actions'] = $Mod->t('actions')
                ->where($where)

                ->join('files', function($join)
                {
                    $join->type = 'LEFT OUTER';
                    $join->on('actions.id', '=','files.id_album')
                        ->where('files.name_table', '=', 'actions');
                })

                ->select('actions.*', 'files.file', 'files.crop')
                ->groupBy('actions.id')
                ->orderBy('actions.id', 'DESC')
                ->paginate(12);

            if((int)$id == 0)
                $data['field'] = 'translation';

            $data['page'] =  $request['page'];

            return $this->base->view_s("shop.main.actions", $data, 'menu_shop');
        }
    }

    public function _sell_out($id, $limit = 4) {
        return $this->dynamic->t('product')
            ->where(['product.active' => 1, 'product.sell_out' => 1, 'product.not_available' => 0])
            ->orWhere(['product.active' => 1, 'product.sell_out' => 0, 'product.not_available' => 0])
            ->whereNotIn('product.id', is_array($id) ? $id : [$id])

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('product.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'product')
                    ->where('files.main', '=', 1);
            })

            ->select('product.*', 'files.file', 'files.crop')
            ->groupBy('product.id')
            ->orderBy($this->dynamic->rand('product.id'))
            ->limit($limit)
            ->get()
            ->toArray();
    }

    /**
     * контакты
     * @return mixed
     */
    public function contacts()
    {
        $data = [];
        $where[] = ['str.active', 1];
        $data['field'] = 'translation';
        $where[] = ['str.translation', 'contacts'];

        $data['page'] = $this->dynamic->t('str')
            ->where($where)

            ->join('files', function($join)
            {
                $join->type = 'LEFT OUTER';
                $join->on('str.id', '=','files.id_album')
                    ->where('files.name_table', '=', 'str');
            })

            ->select('str.*', 'files.file', 'files.crop')
            ->first()
            ->toArray();

        return $this->base->view_s("shop.main.contacts", $data, 'menu_shop');
    }
}
