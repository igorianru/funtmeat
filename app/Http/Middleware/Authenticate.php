<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Request;
use DB;
use \App\User;

class Authenticate {

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //if ($this->auth->guest())
//        if (Request::has("pwd")){
//            $pwd=Request::input("pwd");
//            $allPasswords=DB::table('users')->lists('alias');
//            if (in_array($pwd,$allPasswords)) {
//                $User=User::where("alias",$pwd)->first();
//                Auth::login($User);
//                return $next($request);
//            }
//        }
        if (Auth::check()) {
            return $next($request);
        } else {
            if ($request->ajax())
            {
                return response('Авторизируйтесь пожалуйста', 401);
            }
            else
            {
                return redirect()->guest('/admin/login');
            }
        }

    }

}
