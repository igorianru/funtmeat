var filCat = {
    initialize: function initialize(data) {
        this.conf = data;
        this.url = '/';
        this.cont = this.conf.cont;
        this.num = this.conf.num;
        this.isLoadCat = this.conf.isLoadCat == 'undefined' ? false : this.conf.isLoadCat;
        filCat.loadOnclick()
    },

    // подгружаем обработчки событий
    loadOnclick: function loadOnclick(e) {
        setTimeout(function () {
            $('.typeCooking > div.b > div').click(function() {
                $('.typeCooking > div.b > div').removeClass('active');
                $(this).addClass('active');
                filCat.selectCatalogs();
            });

            $('.timeCooking > div.b > div').click(function() {
                $('.timeCooking > div.b > div').removeClass('active');
                $(this).addClass('active');
                filCat.selectCatalogs();
            });

            $('.typeMeat > div.b > div').click(function() {
                $('.typeMeat > div.b > div').removeClass('active');
                $(this).addClass('active');
                filCat.selectCatalogs();
            });

            $('.filterGroup > div').click(function() {
                $('.filterGroup > div').removeClass('active');
                $(this).addClass('active');
                filCat.selectCatalogs();
            });

            $('.iCheck-helper').click(function() {
                filCat.selectCatalogs();
            });
        }, 100)
    },

    searchGo: function searchGo (e) {
        if(e.which == 13 || e.keyCode == 13)
        {
            filCat.selectCatalogs();
        }
    },
    
    // select catalogs
    selectCatalogs: function () {
        var request, typeCooking, timeCooking, typeMeat, simpleRecipe, filterGroup, inputSearch, page;

        typeCooking = $('.conLSel > div.b > div.active').data('typeCooking');

        if(_.isUndefined(_.toString(typeCooking)))
            typeCooking = -1;

        timeCooking = $('.timeCooking > div.b > div.active').data('timeCooking');

        if(_.isUndefined(timeCooking))
            timeCooking = -1;

        typeMeat = $('.typeMeat > div.b > div.active').data('typeMeat');

        if(_.isUndefined(typeMeat))
            typeMeat = -1;

        filterGroup = $('.filterGroup > div.active').data('filterGroup');

        if(_.isUndefined(filterGroup))
            filterGroup = -1;

        simpleRecipe = $("[name=simple_recipe]:checked").val();
        inputSearch = $("[name=input_search]").val();

        if(_.isUndefined(simpleRecipe))
            simpleRecipe = -1;

        page = $('.pagination').val();

        if(1) {
            request = {
                type_cooking: typeCooking,
                time_cooking: timeCooking,
                type_meat: typeMeat,
                simple_recipe: simpleRecipe,
                filter_group: filterGroup,
                input_search: inputSearch
            };
            console.log('request: ', request)

            $(this.cont).html('<p>Загрузка...</p>');

            $.ajax
            ({
                type: "post",
                url: "/catalog?page=" + page,
                data: request,
                cache: false,
                dataType: "JSON",
                success: function (data) {
                    if(data['result'] == 'ok') {
                        var t = '',  d;
                        for (var i = 0; data.recipe.data.length > i; i++)
                        {
                            d = data.recipe.data[i];
                            t +=
                                '<div class="col-sm-6 col-md-4 recipe-' + d.id + '">' +
                                '<div class="thumbnail cart">' +
                                '<div class="rat">' + d.rating + '</div>';

                            if(_.isEmpty(d.file)) {
                                t += '<img src="/images/files/small/no_img.png"/>';
                            } else {
                                if(d.crop) {
                                    t += '<img src="/images/files/small/' + d.crop +'"/>';
                                } else {
                                    t += '<img src="/images/files/small/' +  d.file +'"/>';
                                }
                            }

                            t +=
                                '<div class="caption">' +
                                '<p>' + d.name + '</p>' +
                                '<div class="col-md-6">' +
                                '<div class="row tim">';

                            t += 'Время: ' + d.time_cooking + ' мин';

                            t +=
                                '</div>' +
                                '</div>' +
                                '<div class="col-md-6 text-right">' +
                                '<div class="row">' +
                                '<a href="/recipe/' + (d.translation || d.id) + '" class="btn btn-red" role="button">Подробнее</a>' +
                                '</div>' +
                                '</div>' +
                                '<div class="clear"></div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                        }

                        t += '<div class="text-center clear">' + data.pagination + '</div>';

                        if(data['recipe'].length == 0) {
                            t = '<p>По таким параметрам ничего не найдено ):</p>';
                        }

                        $(filCat.num).html(data.recipe.total);
                        $(filCat.cont).html(t);
                        console.log('result: ', data)

                        setTimeout(function () {
                            $('.pagination').click(function (ev) {
                                var page = ev.target.innerText;

                                if(data.recipe.current_page == page)  return false;

                                if(!_.isNaN(parseInt(page))) {
                                    $('.pagination').val(page);
                                    filCat.selectCatalogs();
                                }

                                if((data.recipe.current_page + 1) <= data.recipe.last_page) {
                                    if(page == '»') {
                                        nexP = data.recipe.current_page + 1;
                                        $('.pagination').val(nexP);
                                        filCat.selectCatalogs();
                                    }
                                }

                                if((data.recipe.current_page - 1) > 0) {
                                    if(page == '«') {
                                        nexP = data.recipe.current_page - 1;
                                        $('.pagination').val(nexP);
                                        filCat.selectCatalogs();
                                    }
                                }

                                return false;
                            })
                        }, 100)

                    } else {

                    }
                }
            });
        }
    },

    // helper formatting price
    hlPrice: function hlPrice(s) {
        var separator = ' ';

        return s.replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1" + separator);
    },
}