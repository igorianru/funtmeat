var filCat = {
	initialize: function initialize(data) {
		this.conf = data;
		this.url = '/';
		this.cont = this.conf.cont;
		this.num = this.conf.num;
		this.classBox = this.conf.class_box || 'col-md-4 ';
		this.typeMeasure = this.conf.type_measure || ['шт', 'кг', 'л', 'шт', 'шт'];
		this.countBox = this.conf.count_box || 6;
		this.pagination = this.conf.pagination;
		this.isLoadCat = this.conf.isLoadCat == undefined ? true : this.conf.isLoadCat;
		filCat.loadOnclick();
	},

	// подгружаем обработчки событий
	loadOnclick: function loadOnclick(e) {
		setTimeout(function () {
			$('[name=private_house]').on('ifChecked', function (ev) {
				if(ev.target.checked === true)
					$('[name=private_house_h]').val(1)
			});

			$('[name=private_house]').on('ifUnchecked', function (ev) {
				if(ev.target.checked === false)
					$('[name=private_house_h]').val(0)
			});

			$('[data-categories]').click(function() {
				$('.meat_catalog > li > a').removeClass('active');
				$(this).addClass('active');
				$('[name=category]').val($(this).data('categories'));
				filCat.selectCatalogs();
			});

			$('.filterGroup > div').click(function() {
				$('.filterGroup > div').removeClass('active');
				$(this).addClass('active');
				filCat.selectCatalogs();
			});

			$('.basket_dropdown').on('click', function(e) {
				if(!e.target.getAttribute('data-close')) {
					e.preventDefault();
					e.stopPropagation();
				}
			});

			$('.iCheck-helper').click(function() {
				filCat.selectCatalogs();
			});

			// init cart min
			filCat.addToCart(0, 'init');
			
			// select time
			filCat.selectTimeOnClick();

			// send ordering init
			filCat.sendOrdering();
		}, 100)
	},

	sendOrdering: function () {
		var
			vAddress,
			vApartment,
			vComment_1,
			vComment_2,
			vEntrance_floor,
			vEmail,
			vFull_name,
			vPayment_method,
			vPhone;

		function init() {
			vAddress = new Validators.Field({
				check_validate: 'required|min_3',
				element       : document.getElementById('address'),
				only_error    : true,
				nameField     : 'Город, улица, номер дома'
			});

			vApartment = new Validators.Field({
				check_validate: 'required|min_1',
				element       : document.getElementById('apartment'),
				only_error    : true,
				nameField     : 'Квартира или офис'
			});

			vComment_1 = new Validators.Field({
				check_validate: '',
				element       : document.getElementById('comment_1'),
				only_error    : true,
				nameField     : 'Комментарий для курьера'
			});

			vComment_1 = new Validators.Field({
				check_validate: '',
				element       : document.getElementById('comment_1'),
				only_error    : true,
				nameField     : 'Комментарий для курьера'
			});

			vComment_2 = new Validators.Field({
				check_validate: 'required|min_3',
				element       : document.getElementById('comment_2'),
				only_error    : true,
				nameField     : 'Дополнительный комментарий к заказу'
			});

			vEntrance_floor = new Validators.Field({
				check_validate: 'min_1',
				element       : document.getElementById('entrance_floor'),
				only_error    : true,
				nameField     : 'Подъезд/этаж'
			});

			vEmail = new Validators.Field({
				check_validate: 'required|email',
				element       : document.getElementById('email'),
				only_error    : true,
				nameField     : 'Email'
			});

			vFull_name = new Validators.Field({
				check_validate: 'required|min_3',
				element       : document.getElementById('full_name'),
				only_error    : true,
				nameField     : 'Фамилия Имя Отчество'
			});

			vPayment_method = new Validators.Field({
				check_validate: 'required|min_3',
				element       : document.getElementById('payment_method'),
				only_error    : true,
				nameField     : 'Способ оплаты'
			});

			vPhone = new Validators.Field({
				check_validate: 'required|min_3',
				element       : document.getElementById('phone'),
				only_error    : true,
				nameField     : 'Номер телефона'
			});
		}

		filCat.send_app = function () {
			var valid = true;

			vAddress.validate();
			vApartment.validate();
			vEmail.validate();
			vFull_name.validate();
			vPayment_method.validate();
			vPhone.validate();

			setTimeout(function () {
				valid = valid ? vAddress.valid() : valid;
				valid = valid ? vApartment.valid() : valid;
				valid = valid ? vEmail.valid() : valid;
				valid = valid ? vFull_name.valid() : valid;
				valid = valid ? vPayment_method.valid() : valid;
				valid = valid ? vPhone.valid() : valid;


				if(parseInt($('.moneyTop').html()) === 0) {
					$('.mess-ordering').html('<div class="alert alert-danger text-center">' +
						'<h3>Ваша корзина пуста!</h3><br /> <h4>Пожалуйста, перейдите к <a href="/shop/categories/">' +
						'каталог</a> и выберите товар для оформления</h4>' +
						'</div>');

					return false;
				} else
					$('.mess-ordering').html('');

				if(valid) {
					$.ajax({
						cache: false,
						url: '/shop/send_ordering',

						data: {
							area           : $('[name="area"]').val(),
							address        : vAddress.element.value,
							apartment      : vApartment.element.value,
							city           : $('[name="city"]').val(),
							comment_1      : vComment_1.element.value,
							comment_2      : vComment_2.element.value,
							delivery_time_1: $('.time-current-ordering').html(),
							delivery       : $('[name="delivery"]').val(),
							entrance_floor : vEntrance_floor.element.value,
							email          : vEmail.element.value,
							full_name      : vFull_name.element.value,
							payment_method : vPayment_method.element.value,
							phone          : vPhone.element.value,
							private_house  : $('[name=private_house_h]').val(),
						},

						type: 'post',
						dataType: 'JSON',
						success: function (data) {
							if (data.result === 'ok') {
								$('.cont-ordering').html('' +
									'<div style="margin-top: 10px" class="alert alert-success text-center">' +
									'<h2>Успех!</h2>' +
									'<h3>Заказ оформлен успешно! Ожидайте, с Вами свяжутся для уточнения деталей</h3>' +
									'</div>' +
									'<p class="text-center">' +
									'<a class="btn btn-danger" href="/shop/categories">' +
									'<i class="glyphicon glyphicon-arrow-left"></i> ' +
									'Купить ещё' +
									'</a></p>').show(300);

								setTimeout(function () {
									$(".mess_app").hide(300);
									$("#callModal").modal("hide");
								}, 6000);

								vAddress.cssReset(true);
								vApartment.cssReset(true);
								vEmail.cssReset(true);
								vFull_name.cssReset(true);
								vPayment_method.cssReset(true);
								vPhone.cssReset(true);

								// update cart
								filCat.addToCart(0, 'init');
							}
						}
					});
				}
			}, 300);
		};

		if(document.getElementById('address'))
			init()
	},

	searchGo: function searchGo(e) {
		if(e.which === 13 || e.keyCode === 13)
			filCat.selectCatalogs();
	},

	selectTime: function () {
		var time_current = $('.time-value-current').val();

		if(!time_current)
			return false;

		$.ajax
		({
			type: "post",
			url : "/shop/set_time_delivery",

			data: {
				address           : $('.delivery-address-top').val(),
				time_current_day  : $('.time-value-current-day').val(),
				time_current_month: $('.time-value-current-month').val(),
				time_current      : time_current
			},
			cache: false,
			dataType: "JSON",
			success: function (data) {
				if(data.result === 'ok') {
					$('.timeModal .close').click()
					$('.time-current-ordering').html(data.time_current_all)
					$('.time-current-ordering-address').html(data.address)
				}
			}
		})
	},

	selectTimeOnClick: function() {
		$('.table-time .select-time').click(function () {
			var t = $(this), time = t.data('time');

			if(!t.hasClass('disabled')) {
				$('.table-time .select-time').removeClass('active');
				t.addClass('active');
				$('.time-value-current').val(t.data('time'))
				$('.time-value-current-month').val(t.data('month'))
				$('.time-value-current-day').val(t.data('day'))
			}
		})
	},

	/**
	 * plus or minus weight meat
	 * @param id
	 * @param type
	 * @param update
	 */
	poundWeight: function(id, type, update) {
		var currentWeightsCont = $('.product-' + id + ' .weight > span');
		var typeMeasure = parseInt(currentWeightsCont.data('type-measure'));
		var currentPriceCont = $('.product-' + id + ' .price-money > span');
		var priceG = parseFloat(currentPriceCont.data('price-kilogram')) / 1000;
		var weigG, weigKg, counter = 0.1, fixed = 1;

		if(typeMeasure !== 1 && typeMeasure !== 2) {
			counter = 1;
			fixed = 0;
		}

		if(type === 'plus') {
			weigKg = (parseFloat(currentWeightsCont.html()) + counter).toFixed(fixed);
			weigG = (weigKg * 1000).toFixed(0);
			currentWeightsCont.html(weigKg);
			currentPriceCont.html((weigG * priceG).toFixed(2));
		} else {
			weigKg = (parseFloat(currentWeightsCont.html()) - counter).toFixed(fixed);

			if(weigKg >= 0.5) {
				weigG = (weigKg * 1000).toFixed(0);
				currentWeightsCont.html(weigKg);
				currentPriceCont.html((weigG * priceG).toFixed(2));
			}
		}

		if(update)
			this.addToCart(id, 'update')
	},

	addToCart: function(id, type) {
		if(type === 'remove')
			$('.product-cart-shop-' + id).remove();

		$.ajax
		({
			type: "post",
			url: "/shop/add_to_cart",

			data: {
				get_data: true,
				id      : id,
				type    : type,
				weights : (parseFloat($('.product-' + id + ' .weight > span').html()) * 1000).toFixed(2)
			},

			cache: false,
			dataType: "JSON",
			success: function (data) {
				if(data.result === 'ok') {
					var parent = $('.product-' + id);
					var p = $('.shop-cart').offset();

					if(type === 'add') {
						parent
							.clone()
							.addClass('product-' + id + '-tmp product-tmp ')
							.removeClass('col-md-4')

							.css({
								width : parent.width(),
								height: parent.height(),
								top   : parent.offset().top - $('html').scrollTop(),
								left  : parent.offset().left  + 15
							})

							.appendTo('.product-' + id);

						var parentTmp = $('.product-' + id + '-tmp');

						parentTmp.animate({
							left   : p.left - parentTmp.width() / 2.5,
							opacity: .8,
							top    : (p.top - parentTmp.height() / 2.5) - $('html').scrollTop(),
						}, 500);

						parentTmp.addClass('animate');

						function animateAddCart() {
							var div = $('.shop-cart');
							div.animate({opacity: .5}, 300);
							div.animate({opacity: 1}, 150);
						}

						setTimeout(function() { parentTmp.remove(); animateAddCart(); }, 1150);
					}

					if(type === 'update') {
						var tt = '', dd, priceReal;
						for(var ii = 0; data.product.data.length > ii; ii++) {
							dd = data.product.data[ii];
							weights = data.cart[dd.id].weights || 0;
							priceReal =(dd.price_money / 1000) * weights;
							price =((dd.price_money - (dd.price_money / 100 * dd.discount || 0)) / 1000) * weights;

							tt += '<div class="shopping-basket row product-cart-shop-' + dd.id + ' product-' + dd.id + '">' +
								'<div class="col-md-3 col-sm-6 i">' +
								'<a href="javascript:void(0)" onclick="filCat.addToCart(\'' + dd.id + '\', \'remove\')">' +
								'<i class="glyphicon glyphicon-remove"></i>' +
								'</a>' +

								'<a href="/product/' + dd.id + '">';

							if(_.isEmpty(dd.file))
								tt += '<img src="/images/files/small/no_img.png" class="blur1" style="width: 65px; height: 65px" />';
							else
							if(dd.crop)
								tt += '<img src="/images/files/small/' + dd.crop +'" style="width: 65px; height: 65px" />';
							else
								tt += '<img src="/images/files/small/' +  dd.file +'" style="width: 65px; height: 65px" />';

							if(dd.discount)
								tt +='<div class="discount">' + dd.discount + '</div>';

							tt += '</a>' +
								'</div>' +
								'<div class="col-md-3 col-sm-6 t">' +
								'<p>' + dd.name + '</p>' +
								'</div>' +
								'<div class="col-md-3 col-sm-6 c">' +
								'<button type="button" class="btn btn-weight min" onclick="filCat.poundWeight(\'' + dd.id + '\', \'minus\', true)">' +
								'<i class="glyphicon glyphicon-minus"></i>' +
								'</button>' +

								'<span class="weight">' +
								'<span data-type-measure="' + dd.type_measure + '">' + (weights / 1000) + '</span>' +
								filCat.typeMeasure[dd.type_measure] +
								'</span>' +

								'<button type="button" class="btn btn-weight min" onclick="filCat.poundWeight(\'' + dd.id + '\', \'plus\', true)">' +
								'<i class="glyphicon glyphicon-plus"></i>' +
								'</button>' +
								'</div>' +

								'<div class="col-md-3 col-sm-6 p text-right">' +

								'<span>' +
								price +
								'<i class="glyphicon glyphicon-ruble"></i>' +
								'</span>';

							if(dd.discount)
								tt += '<s>' + priceReal + '<i class=" glyphicon glyphicon-ruble"></i></s>';

							tt += '</div>' +
								'</div>';
						}

						$('.basketCont').html(tt);
					}

					var t = '', d, allPrice = 0;
					for(var i = 0; data.product.data.length > i; i++) {
						d = data.product.data[i];
						var weights = data.cart[d.id].weights || 0;
						var price =((d.price_money - (d.price_money / 100 * d.discount || 0)) / 1000) * weights;

						t += '<div class="col-md-12 product-cart-shop-' + d.id + '">' +
							'<div class="shopping-basket">' +
							'<div class="col-xs-6 col-sm-6 col-md-3 i thumbnail">';

						if(_.isEmpty(d.file))
							t += '<img src="/images/files/small/no_img.png" class="blur1" />';
						else
						if(d.crop)
							t += '<img src="/images/files/small/' + d.crop +'"/>';
						else
							t += '<img src="/images/files/small/' +  d.file +'"/>';

						t +='</div>' +
							'<div class="col-xs-6 col-sm-6 col-md-5 t">' +
							'<p>' + d.cat_parent + '</p><span>' + d.name + '</span>' +
							'</div>';

						if(!d.not_available)
							t +='<div class="col-xs-6 col-sm-6 col-md-3 p">' +
								'<span>' + price + '<i class=" glyphicon glyphicon-ruble"></i></span>' +
								'</div>' +
								'<div class="col-xs-6 col-sm-6 col-md-1 c">' +
								'<a href="javascript:void(0)" onclick="filCat.addToCart(' + d.id + ', \'remove\')">' +
								'<i class="glyphicon glyphicon-remove"></i>' +
								'</a>' +
								'</div>';

						t +='</div>' +
							'</div>';

						allPrice = allPrice + price;
					}

					if(type !== 'remove')
						$('#basketCont').html(t);

					$('.basket_dropdown > .a').removeClass('hidden');
					$('.btn-link-time').removeClass('hidden');
					$('.basket_dropdown_btn > .b > span').html(allPrice);
					$('.moneyTop').html(allPrice);

					if(!data.product.data.length) {
						$('.basket_dropdown > .a').addClass('hidden');
						$('.shop-cart > .p').addClass('hidden');
						$('.btn-link-time').addClass('hidden');
						$('.basketCont').html('<p>Корзина пуста</p><img src="/images/shop/logo.png" class="bgCategories">');
						$('#basketCont').html('<p>Корзина пуста</p>');
						$('.moneyTop').html(0);
					} else
						$('.shop-cart > .p').html(data.product.data.length).removeClass('hidden')
				}
			}
		})
	},

	// select catalogs
	selectCatalogs: function () {
		if(!this.isLoadCat)
			return false;

		var request, category, filterGroup, inputSearch, page;

		filterGroup = $('.filterGroup > div.active').data('filterGroup');

		if(_.isUndefined(filterGroup))
			filterGroup = -1;

		category = $('[name=category]').val();

		if(_.isUndefined(category))
			category = -1;

		page = $('.pagination').val();
		inputSearch = $('[name="input_search"]').val() || '';

		if(1) {
			request = {
				actions       : $("[name=actions]:checked").val() || -1,
				category      : category,
				count_box     : this.countBox,
				filter_group  : filterGroup,
				input_search  : inputSearch,
				order_by      : this.conf.order_by || '',
				seasonal_goods: $("[name=seasonal_goods]:checked").val() || -1,
			};

			$(this.cont).css({opacity: 0.3});

			$.ajax
			({
				type: "post",
				url: "/shop/categories?page=" + page,
				data: request,
				cache: false,
				dataType: "JSON",
				success: function (data) {
					if(data.result === 'ok') {
						var t = '',  d;
						for (var i = 0; data.product.data.length > i; i++)
						{
							d = data.product.data[i];

							var counter = 1.0, fixed = 1;

							if(d.type_measure !== 1 && d.type_measure !== 2) {
								counter = 1;
								fixed = 0;
							}

							t += '<div class="' + filCat.classBox + ' product product-' + d.id + '">' +
								'<div class="shop-cart-big">' +
								'<div>' +
								'<div class="t thumbnail">' +
								'<a href="/shop/product/' + d.id + '">';

							if(_.isEmpty(d.file))
								t += '<img src="/images/files/small/no_img.png" class="blur1" />';
							else
								if(d.crop)
									t += '<img src="/images/files/small/' + d.crop +'"/>';
								else
									t += '<img src="/images/files/small/' +  d.file +'"/>';

							t += '</a>' +
								'</div>' +
								'<div class="b">' +
								'<div class="z">' + d.name + '</div>';

							if(!d.not_available)
								t += '<div class="m">' +
									'<button type="button" class="btn" onclick="filCat.poundWeight(' + d.id + ', \'minus\')">' +
									'<i class="glyphicon glyphicon-minus"></i>' +
									'</button>' +
									'<span class="weight"><span data-type-measure="' + d.type_measure + '">' +
									counter.toFixed(fixed) +
									'</span> ' + filCat.typeMeasure[d.type_measure] + '</span>' +
									'<button type="button" class="btn" onclick="filCat.poundWeight(' + d.id + ', \'plus\')">' +
									'<i class="glyphicon glyphicon-plus"></i>' +
									'</button>' +
									'<div class="p">' +
									'<div class="col-md-9 text-left">' +
									'<div class="row">' +
									'<span class="price-money">' +
									'<span data-price-kilogram="' + d.price_money + '">' + d.price_money + '</span> <i class="glyphicon glyphicon-ruble"></i>' +
									'</span>' +
									'</div>' +
									'</div>' +
									'<div class="col-md-3 text-right">' +
									'<div class="row">' +
									'<a href="javascript:void(0)" onclick="filCat.addToCart(' + d.id + ', \'add\')">' +
									'<img class="imgbb" src="/images/shop/shopping_cart.png" title="Добавить в корзину">' +
									'</a>' +
									'</div>' +
									'</div>' +
									'</div>' +
									'</div>';
							else
								t += '<div class="m" style="height: 122px;display: inline-flex;">' +
									'<b style="color: #da2726; margin-top: calc(50% - 10px);">Нет в наличии</b>' +
									'</div>';

								t += '</div>' +
								'</div>' +
								'</div>' +
								'</div>';
						}

						if(filCat.pagination)
							t += '<hr class="clear"/><div class="catalog_button text-center">' + data.pagination + '</div>';

						if(!data.product.total)
							t = '<p>По таким параметрам ничего не найдено ):' +
								'<img src="/images/shop/logo.png" class="bgCategories"/></p>';

						$(filCat.num).html(data.product.total);
						$(filCat.cont).html(t);
						console.log('result: ', data)

						setTimeout(function () {
							$('.pagination').click(function (ev) {
								var page = ev.target.innerText;

								if(data.product.current_page == page)  return false;

								if(!_.isNaN(parseInt(page))) {
									$('.pagination').val(page);
									filCat.selectCatalogs();
								}

								if((data.product.current_page + 1) <= data.product.last_page) {
									if(page == '»') {
										nexP = data.product.current_page + 1;
										$('.pagination').val(nexP);
										filCat.selectCatalogs();
									}
								}

								if((data.product.current_page - 1) > 0) {
									if(page == '«') {
										nexP = data.product.current_page - 1;
										$('.pagination').val(nexP);
										filCat.selectCatalogs();
									}
								}

								return false;
							})
						}, 100)

					} else {

					}

					$(filCat.cont).animate({opacity: 1}, 150);
				}
			});
		}
	},

	// helper formatting price
	hlPrice: function hlPrice(s) {
		var separator = ' ';

		return s.replace(/(\d{1,3}(?=(\d{3})+(?:\.\d|\b)))/g,"\$1" + separator);
	},
}