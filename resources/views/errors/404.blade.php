<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="404 not found" />
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta http-equiv="Last-Modified" content="Fri, 11 Sep 2015 11:20:27">


    <link rel="image_src" href="" />

    <link rel="icon" type="image/x-icon" href="/images/site/logoF.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#e30079">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>404 not found</title>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="http://poundmeat.ru/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://poundmeat.ru/css/style.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://poundmeat.ru/js/bootstrap/js/bootstrap.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="/css/admin_site.css" rel="stylesheet" type="text/css" />

    <link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
    <script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
    <script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
    <link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">
</head>

<body>
<style>
    html, body {
        min-height: 100%;
        height: 100%;
    }
</style>

<div class="container" style=" position: absolute; top: calc((100%/2) - 180px) ; left: 0; width: 100%">
    <div class="title" style="
                text-align: center;
                font-size: 80px; font-weight: 100; width: 100%">
        404 not found
        <br />
        <a href="/" style="margin: 0 auto">
            <img src="/images/site/logo.png" />
        </a>
    </div>
</div>




</body>
</html>