@extends('site.layouts.default')
@section('header', '
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>')

@section('content')

    <style>
        #map {
            height: 400px;
            width: 100%;
            position: absolute;
            z-index: 1;
        }

        .slide-div {
            height: 410px;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="container-map text-center">
                <div class=" slide-div">
                    <div id="map"></div>
                    <script>
                        var map;
                        function initMap() {
                            map = new google.maps.Map(document.getElementById('map'), {
                                center: {lat: 48.769969, lng: 44.613539},
                                zoom: 11,
                                scrollwheel: true
                            });

                            var marker = new google.maps.Marker({
                                position: new google.maps.LatLng('48.769969', '44.813539'),
                                map: map,
                                title: 'Фунт Мяса',
                                icon: '/images/site/ic/point.png'
                            });

                            var marker2 = new google.maps.Marker({
                                position: new google.maps.LatLng('48.755963', '44.501437'),
                                map: map,
                                title: 'Фунт Мяса',
                                icon: '/images/site/ic/point.png'
                            });

                            var marker3 = new google.maps.Marker({
                                position: new google.maps.LatLng('48.721423', '44.534483'),
                                map: map,
                                title: 'Фунт Мяса',
                                icon: '/images/site/ic/point.png'
                            });

                            var contentString = '<div id="content" class="text-left">' +
                                    '<h4>"Фунт Мяса. Волжский"</h4><p>' +
                                    '<b>адрес: </b>Оломоуцкая, 45н.</p>';

                            var contentString2 = '<div id="content" class="text-left">' +
                                    '<h4>"Фунт Мяса. Дзержинский"</h4><p>' +
                                    '<b>адрес:</b> Ул. 8-й Воздушной Армии, 37а</p>';

                            var contentString3 = '<div id="content" class="text-left">' +
                                    '<h4>"Фунт Мяса. Центральный"</h4><p>' +
                                    '<b>адрес:</b> Пр. Ленина 48</p>';


                            var infowindow = new google.maps.InfoWindow({
                                content: contentString,
                                map: map,
                                disableAutoPan: false
                            });
                            infowindow.close(map, marker);
                            marker.addListener('click', function() {
                                infowindow.open(map, marker);
                            });

                            var infowindow2 = new google.maps.InfoWindow({
                                content: contentString2,
                                map: map,
                                disableAutoPan: true
                            });

                            infowindow2.close(map, marker2);
                            marker2.addListener('click', function() {
                                infowindow2.open(map, marker2);
                            });

                            /* 3 */
                            var infowindow3 = new google.maps.InfoWindow({
                                content: contentString3,
                                map: map,
                                disableAutoPan: true
                            });

                            infowindow3.close(map, marker3);
                            marker3.addListener('click', function() {
                                infowindow3.open(map, marker3);
                            });
                            /* 3 */

                            $(".infowindow1").click (function() {
                                infowindow2.close(map, marker2);
                                infowindow.open(map, marker);
								infowindow3.close(map, marker3);
                                map.panTo(new google.maps.LatLng('48.769969', '44.813539'));
                            });

                            $(".infowindow2").click (function() {
                                infowindow.close(map, marker);
                                infowindow2.open(map, marker2);
								infowindow3.close(map, marker3);
                                map.panTo(new google.maps.LatLng('48.755963', '44.501437'));
                            });

                            $(".infowindow3").click (function() {
                                infowindow.close(map, marker);
                                infowindow2.close(map, marker2);
                                infowindow3.open(map, marker3);
                                map.panTo(new google.maps.LatLng('48.721423', '44.534483'));
                            });
                        }
                    </script>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0GGmusq8RhpCCggatJxq8WoCNu-_-hxs &callback=initMap" async defer></script>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="con" style="min-height: 500px">
            <p class="clear">
                <span class="titCooR">{{ $data[0]['title'] or '' }}</span>
            </p>
            <br class="clear"/>

            <div class="cont_text">{!! $data[0]['text'] or '' !!}</div>
            <div class="row">
                <div class="col-md-4">
                    <b>"Фунт Мяса. Волжский"</b><br />
                    Адрес: Оломоуцкая 45н<br />
                    Режим работы: с 8:00 до 20:00<br />
                    Телефон: +7 (927) 510-77-75<br />
                    Телефон: +7 (8442) 50-77-75<br />

                    <p class="text-right">
                        <button class="btn btn-default infowindow1">Показать</button>
                        <button class="btn btn-red" onclick="href('/page/11')">Подробнее</button>
                    </p>
                </div>
                <div class="col-md-4">
                    <b>"Фунт Мяса. Дзержинский"</b><br />
                    Адрес: 8ая воздушная 37а<br />
                    Режим работы: с 8:00 до 20:00<br />
                    Телефон: +7 (8442) 50-73-77<br />
                    <br />
                    <p class="text-right">
                        <button class="btn btn-default infowindow2">Показать</button>
                        <button class="btn btn-red" onclick="href('/page/12')">Подробнее</button>
                    </p>
                </div>
                <div class="col-md-4">
                    <b>"Фунт Мяса. Центральный"</b><br />
                    Адрес: Пр. Ленина, 48<br />
                    Режим работы: с 8:00 до 20:00<br />
                    Телефон: +7 (8442) 50-07-73<br />
                    <br />
                    <p class="text-right">
                        <button class="btn btn-default infowindow3">Показать</button>
                        <button class="btn btn-red" onclick="href('/page/13')">Подробнее</button>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{--address--}}
                </div>
                <div class="col-md-6">
                    {{--address--}}
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>


@stop