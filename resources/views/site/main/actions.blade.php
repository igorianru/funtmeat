@extends('site.layouts.default')
@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">
')

@section('content')
    <div class="container-fluid mainR adviceR"
         style="
             {{--background-image: url('/images/files/big/{{ isset($actions[0]->file) ? $actions[0]->file : '' }}');--}}
             background-repeat: no-repeat;
             background-position: 50% 33%;
             background-color: #a1232e;
             background-size: 100%;"
    >
        <div class="row" style="background: rgba(0, 0, 0, .3)">
            <div class="container">
                <div class="text-mainR">
                    <div class="cont_text">
                        <ol class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active">Акции</li>
                        </ol>
                    </div>
                    <div class="titF">
                        Акции
                    </div>
                    @if(!$page || $page == 1)
                        <div style="min-height: 400px; position: relative">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <?php
                                    $i = 0;
                                    foreach ($actions as $a) {
                                    if($i < 3) {
                                    if($i == 0) { $class =  'active';} else { $class = ''; }
                                    ?>
                                    <div class="item {{ $class }}">
                                        <div class="carousel-caption1">
                                            <div class="nameM">{{ $a->name }}</div>

                                            <div class="col-md-9" style="text-align: justify;min-height: 300px;">
                                                <p>
                                                <?php
                                                $stringAll = $string = strip_tags($a->text);
                                                $string = substr($string, 0, 800);
                                                $string = rtrim($string, "!,.-");
                                                $string = substr($string, 0, strrpos($string, ' '));

                                                if(strlen($stringAll) > 800) {
                                                    echo $string."…";
                                                } else {
                                                    echo $string;
                                                }
                                                ?>

                                                <div style='height: 35px'>
                                                    <a style="text-decoration: underline;color: #fff; float: right" href='/actions?id={{ $a->id }}'>Подробнее..</a>
                                                </div>
                                                </p>
                                            </div>
                                            <div class="col-md-3" style="">
                                                <img src="/images/site/council/council_1.png"/>
                                            </div>
                                            <p class="text-right clear">
                                                <br />
                                            </p>
                                        </div>
                                    </div>
                                    <?php }
                                    $i++;  } ?>
                                </div>
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <?php
                                    $i = 0;
                                    foreach ($actions as $a) {
                                    if($i < 3) {
                                    if($i == 0) { $class =  'active';} else { $class = ''; }
                                    ?>
                                    <li data-target="#carousel-example-generic" data-slide-to="{{ $i }}" class="{{ $class }}"></li>
                                    <?php }
                                    $i++;  } ?>
                                </ol>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="min-height: 700px">
        <div class="con">
            <div class="content_scr">
                <div class="" style="min-height: 600px">
                    {!!  $data[0]['text'] or '' !!}

                    @foreach($actions as $f)
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail cart">
                                @if($f->file)
                                    <img style="max-height: 170px; width: 100%" src="/images/files/small/{{ $f->file }}">
                                @else
                                    <div class="img"></div>
                                @endif
                                <div class="caption">
                                    <p>{{ $f->name }}</p>
                                    <div class="col-md-12 text-right">
                                        <div class="row">
                                            <a href="/actions?id={{ $a->id }}" class="btn btn-red" role="button">Подробнее</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="clear text-center">
                    {!! $actions->render() !!}
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>

    <script>
		$('.carousel').carousel({
			interval: 15000
		})
    </script>
@stop