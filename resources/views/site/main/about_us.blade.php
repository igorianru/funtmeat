@extends('site.layouts.default')
@section('header', '
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>')

@section('content')

    <style>
        .fonAboutUs {
            background-image: url("/images/site/fon_05.jpg");
            background-repeat: no-repeat;
            background-position: bottom ;
            background-size: 100%;
            width: 100%;
            height: 1000px;
        }

        @media (max-width: 1199px) {
            .fonAboutUs {
                height: 700px;
            }
        }

        @media (max-width: 991px) {
            .fonAboutUs {
                height: 700px;
            }
        }

        @media (max-width: 768px) {
            .fonAboutUs {
                height: 500px;
            }
        }



        .breadcrumb {
            margin-top: 20px;
            background: none;
            padding: 8px 0;
        }

        .breadcrumb a, .breadcrumb li {
            color: #717171;
            font-size: 14px;
        }

        .titBig {
            font-size: 58px;
            line-height: 58px;
            font-weight: 600;
            color: #252525;
        }

        .cont_text {
            font-size: 24px;
            color: #252525;
        }

    </style>

    <div class="container" style="background: none">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#">Главная</a></li>
                <li class="active">{{ $data[0]['name'] or '' }}</li>
            </ol>
            <p class="clear">
                <span class="titBig">{{ $data[0]['name'] or '' }}</span>
            </p>

            <div class="cont_text">{!! $about->text or '' !!}</div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="fonAboutUs"></div>


@stop