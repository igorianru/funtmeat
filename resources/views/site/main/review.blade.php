@extends('site.layouts.default')
@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">
')

@section('content')
    <div class="container">
        <div class="row text-mainR">
            <div class="cont_text">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Отзывы</li>
                </ol>
            </div>
            <div class="titB">
                Отзывы
            </div>
        </div>
    </div>
    <div class="container" style="min-height: 700px">
        <div class="con">
            <div class="content_scr" style="margin-top: 0">
                <div class="" style="min-height: 600px">
                    {!!  $data[0]['text'] or '' !!}

                    @foreach($review as $f)
                        <div class="col-sm-12 col-md-12">
                            <div class="thumbnail">
                                @if($f->file)
                                    <img style="max-height: 170px; width: 100%" src="/images/files/small/{{ $f->file }}">
                                @else
                                    {{--<div class="img"></div>--}}
                                @endif
                                <p class="caption">
                                <p class="text-right" style="float: right">{{ $f->name }}</p>
                                @php($ds = new DateTime($f->created_at))
                                <p class="text-left" style="float: left; color: #a2a2a2">{{ $ds->format("d-m-Y") }}</p>
                                <div class="clear"></div>

                                <div class="text-justify">
                                    @php(
                                     $stringAll = $string = strip_tags($f->text);
                                     $string = mb_substr($string, 0, 400, 'UTF-8');
                                     $string = rtrim($string, "!,.-");

                                     if(strlen($stringAll) > 800) {
                                         echo $string."…";
                                     } else {
                                         echo $string;
                                     })

                                    <p style='height: 35px'>
                                        <a style="text-decoration: underline; float: right" href='/review?id={{ $f->id }}'>Подробнее..</a>
                                    </p>
                                </div>
                                <hr class="clear" />
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="clear text-center">
                    {!! $review->render() !!}
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <script>
		$('.carousel').carousel({
			interval: 15000
		})
    </script>
@stop