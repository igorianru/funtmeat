@extends('site.layouts.default')


@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">
')

@section('content')
    <div class="container-fluid">
        <div class="con">
            <div class="cont_tit">{{ $data[0]->name or '' }}</div>
            <div class="cont_text">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">{{ $data[0]->name or '' }}</li>
                </ol>
            </div>

            <div class="content_scr">
                <div class="">
                    <div class="scrollbar-inner">
                        <!--t sl-->
                @foreach($news as $v)
                    <div onclick="href('/news/{{ $v->id }}')" class="col-sm-4 col-md-3" style="padding-left: 2px; padding-right: 2px">
                        <div class="thumbnail cont_album" >
                            @php($ds = new DateTime($v->date_start))

                            <div class="d">{{ $ds->format("d-m-Y") }}</div>
                                <div style="background-size:cover;background-position: center;width: 100%; height: 250px;

                                        background-image: url('@if($v->crop) {{ $v->crop }} @else {{ '/images/files/small/' . $v->file }} @endif')">

                                </div>
                                <div style="height: 50px; width: 100%"></div>

                            <div class="captions">
                                <div class="col-md-12 t">
                                    <h4>{{ $v->name }}</h4>
                                </div>
                                <div class="b">
                                    <div class="col-sm-8">
                                        <div class="text">
                                            {!! $v->little_description !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="">
                                            <p><a href="/news/{{ $v->id }}" class="btn btp-more btn-block" role="button">Подробнее</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <script>
        ymaps.ready(init);

        function init () {
            var map = new ymaps.Map('map', {
                        center: [54.489179, 36.189475],
                        zoom: 17,
                        controls: []
                    }),
                    counter = 0,

            // Создание макета содержимого балуна.
            // Макет создается с помощью фабрики макетов с помощью текстового шаблона.
                    BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
                            '<div style="padding: 10px 0;">' +
                            '<b style="color: #337ab7; font-size: 1.2em; display: block; padding-bottom: 10px; margin-bottom: 10px; border-bottom: 1px solid #ddd;">@{{properties.name}}</b>' +
                            '<span style="color: #aaa; font-size: 0.9em; display: inline-block; width: 20px;">&#127968;</span> @{{properties.address}}<br>' +
                            '<span style="color: #aaa; font-size: 0.9em; display: inline-block; width: 20px;">&#128222;</span> @{{properties.phone}}<br>' +
                            '<span font-size: 0.8em;><span style="color: #aaa; font-size: 0.9em; display: inline-block; width: 20px;">&#128345;</span> <em>@{{properties.worktime}}</em></span>' +
                            '</div>' );



            var placemark_vlg_city = new ymaps.Placemark([54.489179, 36.189475], {
                balloonContentBody: [
                    '<address style="font-style:normal;font-family: \'PTSansRegular\';font-size: 0.9em;color: #424242;">',
                    '<strong style="font-weight: 600;font-style:normal;">Отдел продаж</strong>',
                    '<br/>',
                    '<b style="font-weight: 600;font-style:normal;">Адрес</b>: г. Калуга, Правый Берег, ул. Фомушина 31',
                    '<br/>',
                    'офис 16 г',
//                    '<br/>',
//                    '<b style="font-weight: 600;font-style:normal;">График работы:</b> c 9:00 до 19:00, пн-пт',
                    '<br/>',
                    '<b style="font-weight: 600;font-style:normal;">тел.</b> +7 909 252 15 62 <br/>',
                    '<b style="font-weight: 600;font-style:normal;">тел.</b> +7 961 005 67 32<br/>',
                    '<b style="font-weight: 600;font-style:normal;">тел.</b> +7 905 642 88 62<br/>',
                    '</address>'
                ].join('')
            }, {
                //balloonContentLayout: BalloonContentLayout,
                // balloonPanelMaxMapArea: 0
                preset: 'islands#redDotIcon'
            });



            // map.behaviors.disable('scrollZoom');
            //map.behaviors.disable('searchControl');

            map.behaviors.enable('drag');
            map.geoObjects.add(placemark_vlg_city);

            placemark_vlg_city.balloon.open();
            // placemark_aquamarin.balloon.open();


        }


        $('#map').css({height: $(window).height() - 450});
        $(window).resize(function () {
            $('#map').css({height: $(window).height() - 450});
        });
    </script>

@stop