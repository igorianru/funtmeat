@extends('site.layouts.default')
{{--@section('title',"bananda - весь город дарит тебе подарки на день рождения! / Главная")--}}

@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>')

@section('content')
    @include('site.block.slider_main')
    <div class="container-fluid mainB">
        <div class="row" style="background: rgba(255, 255, 255, 0.8)">
            <div class="container">
                <div class="row text-mainB">
                    <div class="titB MaB30">
                        История компании
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            {{ isset($params['text_main']) ? $params['text_main']['key'] : '' }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--<div class="container-fluid mainB" style="background: none">--}}
        {{--<div class="row" style="background: rgba(255,153,34,0.8)">--}}
            {{--<div class="container">--}}
                {{--<div class="row text-mainB ">--}}
                    {{--<div class="titB MaB30">--}}
                        {{--Закупаем мясо домашней птицы--}}
                    {{--</div>--}}
                    {{--<div class="text-center">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<p>Закупаем мясо домашней птицы (курица, индейка, утка, гусь, индоутка и так далее)</p>--}}
                            {{--<b>По вопросам поставок звонить 8 (917) 834 80-70</b>--}}

                            {{--<br /><br />--}}

                            {{--<p class="clear thumbnail" style="background: none">--}}
                                {{--<img src="/images/site/frBOGMS8fS8.jpg" style="max-height: 450px; margin: 0 auto" />--}}
                            {{--</p>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="container-fluid mainB" style="background: none;">
        <div class="row" style="background: rgba(255,153,34,0.8)">
            <div class="container">
                <div class="row text-mainB ">
                    <div class="text-center">
                        <div class="col-md-12">
                            {!! isset($params['main_dynamic_text']) ? $params['main_dynamic_text']['key'] : '' !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid mainR">
        <div class="container">
            <div class="row text-mainR">
                <div class="titF MaB30">
                    Полезные советы
                </div>
                <div style="min-height: 400px; position: relative">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                            $i = 0;
                            foreach ($advice as $a) {
                            if($i < 3) {
                            if($i == 0) { $class =  'active';} else { $class = ''; }
                            ?>
                            <div class="item {{ $class }}">
                                <div class="carousel-caption1">
                                    <div class="nameM">{{ $a->name }}</div>
                                    <div class="col-md-9" style="text-align: justify;min-height: 280px;">
                                        <p>
                                            <?php
                                            $stringAll = $string = strip_tags($a->text);
                                            $string = mb_substr($string, 0, 500, 'UTF-8');
                                            $string = rtrim($string, ",-");

                                            if(strlen($stringAll) > 800) {
                                                echo $string."…
                                                            <p style='height: 35px'>
                                                               <a style=\"text-decoration: underline;color: #fff; float: right\" href='/advice/". $a->cat ."?id=". $a->id ."'>Подробнее..</a>
                                                               </p>";
                                            } else {
                                                echo $string;
                                            }
                                            ?>
                                        </p>
                                    </div>
                                    <div class="col-md-3" style="">
                                        @if($a->file)
                                            @if($a->crop)
                                                <img style="width: 277px;" src="/images/files/small/{{ $a->crop }}" class="imgSL"/>
                                            @else
                                                <img style="width: 277px;" src="/images/files/small/{{ $a->file }}" class="imgSL"/>
                                            @endif
                                        @else
                                            <img src="/images/site/council/council_1.png" class="imgSL"/>
                                        @endif
                                    </div>
                                    <p class="text-right-Mc clear">
                                        <br />
                                        <button class="btn btn-main" onclick="href('/advice/4')">Больше советов</button>
                                    </p>
                                </div>
                            </div>
                            <?php }
                            $i++;  } ?>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php
                            $i = 0;
                            foreach ($advice as $a) {
                            if($i < 3) {
                            if($i == 0) { $class =  'active';} else { $class = ''; }
                            ?>
                            <li data-target="#carousel-example-generic" data-slide-to="{{ $i }}" class="{{ $class }}"></li>
                            <?php }
                            $i++;  } ?>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="container-fluid mainIm">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="mainImD">--}}

                    {{--<div class="col-xs-12 col-sm-6 col-md-6">--}}
                        {{--<div class="t">--}}
                            {{--Готовим Вкусно--}}
                        {{--</div>--}}
                        {{--<div class="b">--}}
                            {{--Мастер-классы--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-12 col-sm-6 col-md-6 text-right-Mc r">--}}
                        {{--<button class="btn btn-main">Подробнее</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="container-fluid ">
        <div class="container">
            <div class="row">
                <div class="mainBT">
                    <div class="t">
                        Рецепты блюд
                    </div>
                    <div class="c shGh-div">
                        <div class="col-md-8">
                            <div class="nameM">{{ $recipe['name'] }}</div>
                            <div class="text-main">
                                {{ $recipe['little_description'] }}
                            </div>
                            <div class="progress-main">
                                <div>
                                    <div class="l">
                                        Сложность
                                    </div>
                                    <div class="r">
                                        <div class="progress">
                                            <div
                                                class="progress-bar progress-bar-success"
                                                role="progressbar"
                                                aria-valuenow="{{ $recipe['simple_recipe'] }}"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 40%"
                                            >
                                                <span class="sr-only">
                                                    {{ $recipe['simple_recipe'] }}% Complete (success)
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="progress-main">
                                <div>
                                    <div class="l">
                                        Время
                                    </div>
                                    <div class="r">
                                        <div class="progress">
                                            <div
                                                class="progress-bar progress-bar-success"
                                                role="progressbar"
                                                aria-valuenow="{{ $recipe['time_cooking'] }}"
                                                aria-valuemin="0"
                                                aria-valuemax="100"
                                                style="width: 50%"
                                            >
                                                <span class="sr-only">
                                                    {{ $recipe['time_cooking'] }}% Complete (success)
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        @if($recipe['file'])
                            <div class="col-md-4 text-right">
                                <div
                                    class="rating-main"
                                    style="
                                            background-image: url('/images/files/small/{{ $recipe['file'] }}');
                                            background-size: cover;
                                            background-position: center;
                                          "
                                >
                                    <div>
                                        <div class="t">
                                            {{ $recipe['rating'] }}
                                        </div>
                                        <div class="b">
                                            рейтинг
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="col-xs-12 col-md-4 text-right">
                                <div class="rating-main">
                                    <div>
                                        <div class="t">
                                            {{ $recipe['rating'] }}
                                        </div>
                                        <div class="b">
                                            рейтинг
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-12 b clear">
                        <div class="shGh-div" style="margin-bottom: 20px">
                            <div style="   overflow: hidden;">
                                {!! $recipe['text'] !!}
                            </div>
                            <div class="titAll text-right-Mc">
                                Приятного аппетита!
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <button class="btn btn-main color-red shGh" data-show="true">
                                    <span>Свернуть</span>
                                    <i class="glyphicon glyphicon-triangle-top"></i>
                                </button>
                            </div>
                            <div class="col-md-6 text-right-Mc">
                                <button onclick="href('/catalog')" class="btn btn-main color-red">
                                    Больше рецептов
                                </button>
                            </div>
                        </div>

                        <script>
                            $(document).ready(function () {
                                $('.shGh').click(function () {
                                    var shGh = $(this).attr('data-show');

                                    if(shGh == 'true') {
                                        $('.shGh-div').slideUp(500);
                                        $(this).attr('data-show', 'false');
                                        $(this).children('span').html('Развернуть');
                                        $(this).children('i').attr('class', 'glyphicon glyphicon-triangle-bottom');
                                    } else {
                                        $('.shGh-div').slideDown(500);
                                        $(this).attr('data-show', 'true');
                                        $(this).children('span').html('Свернуть');
                                        $(this).children('i').attr('class', 'glyphicon glyphicon-triangle-top');
                                    }
                                })
                            })
                        </script>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.carousel').carousel({
            interval: false
        })
    </script>
@stop