@extends('site.layouts.default')


@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">
')

@section('content')
    <div class="container-fluid">
        <div class="con">
            <div class="cont_tit">{{ $data[0]->name or '' }}</div>
            <div class="cont_text">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">{{ $data[0]->name or '' }}</li>
                </ol>
            </div>

            <div class="content_scr">
                <div class="">
                    <div class="scrollbar-inner">
                        <!--t sl-->
                        <div class="cont_text">
                            @foreach($page as $v)
                                <?php if($v['translation'] == '') { ?>
                                <div onclick="href('/page/{{ $v->id }}')" class="col-sm-4 col-md-3" style="padding-left: 2px; padding-right: 2px">
                                    <?php } else { ?>
                                    <div onclick="href('/статьи/{{ $v->translation }}')" class="col-sm-4 col-md-3" style="padding-left: 2px; padding-right: 2px">
                                        <?php } ?>
                                        <div class="thumbnail cont_album">
                                            @php($ds = new DateTime($v->date_start))
                                            <div class="d">{{ $ds->format("d-m-Y") }}</div>
                                            <div style="background-size:cover;background-position: center;width: 100%; height: 250px;

                                                    background-image: url('@if($v->crop) {{ $v->crop }} @else {{ '/images/files/small/' . $v->file }} @endif')">

                                            </div>
                                            <div style="height: 50px; width: 100%"></div>

                                            <div class="captions">
                                                <div class="col-md-12 t">
                                                    <h4>{{ $v->name }}</h4>
                                                </div>
                                                <div class="b">
                                                    <div class="col-sm-8">
                                                        <div class="text">
                                                            {!! $v->little_description !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="">
                                                            <p>
                                                            <?php if($v['translation'] == '') { ?>
                                                                <a href="/page/{{ $v->id }}" class="btn btp-more btn-block" role="button">Подробнее</a></p>
                                                            <?php } else { ?>
                                                                <a href="/статьи/{{ $v->translation }}"
                                                               class="btn btp-more btn-block" role="button">Подробнее</a>
                                                            <?php } ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
@stop