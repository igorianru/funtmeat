@extends('site.layouts.default')


@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">
')

@section('content')
    <div class="container">
        <div class="con">
{{--            <div class="cont_tit">{{ $data[0]->name or '' }}</div>--}}
            <div class="cont_text">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">{{ $data[0]->name or '' }}</li>
                </ol>
            </div>

            <div class="content_scr" style="margin-top: 0;">
                <div class="">
                    <div class="scrollbar-inner">
                        <!--t sl-->
                        @foreach($album as $v)
                            <div onclick="href('/gallery/{{ $v->id }}')" class="col-sm-4 col-md-3" style="padding-left: 2px; padding-right: 2px">
                                <div class="thumbnail cont_album cart">
                                    @php($ds = new DateTime($v->date_start))
                                    <div class="d">{{ $ds->format("d-m-Y") }}</div>
                                    @if($v->file)
                                        @if($v->crop)
                                            <img src="{{ $v->crop }}" style="width: 100%;"/>
                                        @else
                                            <img src="/images/files/small/{{ $v->file }}" style="width: 100%;"/>
                                        @endif
                                    @else
                                        <img src="" alt="..." style="height: 250px">
                                    @endif
                                    <div class="captions">
                                        <div class="col-md-12 t">
                                            <h4>{{ $v->name }}</h4>
                                        </div>
                                        <div class="b">
                                            {{--<div class="col-sm-5">--}}
                                                {{--<div class="text">--}}
{{--                                                    {!! $v->text !!}--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            <div class="col-sm-12 text-right">
                                                <div class="row">
                                                    <p><a href="/gallery/{{ $v->id }}" class="btn btn-red" role="button">Подробнее</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
@stop