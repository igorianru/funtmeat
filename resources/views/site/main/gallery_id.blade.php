@extends('site.layouts.default')


@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">
')

@section('title', $data[0]['name'])

@section('content')


    <!-- Modal -->
    <div class="modal fade modalgal" id="galleryc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-body text-center">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{--<h4 class="modal-title" id="myModalLabel">Обратный звонок</h4>--}}
                    </div>
                    <div id="carousel-example-captions" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">

                            @foreach($files as $v)
                                <div class="item thumbnail item-{{ $v->id  }}" style="margin-bottom: 0">
                                    <img data-holder-rendered="true" src="/images/files/big/{{ $v->file }}" style="margin: 0 auto">
                                    <div class="carousel-caption">
                                        <h3>{{ $v->name  }}</h3>
                                        {{ $v->text  }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <a class="mleft left carousel-control" href="#carousel-example-captions" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="mright right carousel-control" href="#carousel-example-captions" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="con">
            <div class="cont_tit">{{ $data[0]->name or '' }}</div>
            <div class="cont_text">
                <ol class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/gallery">Галерея</a></li>
                    <li class="active">{{ $album[0]->name or '' }}</li>
                </ol>
            </div>

            <div class="content_scr" style="margin-top: 0;">
                <div class="">
                    <div class="scrollbar-inner">
                        <!--t sl-->
                        <div class="body-c">
                            <p>{!! $album[0]->text or '' !!}</p>
                            @foreach($files as $v)
                                <div data-toggle="modal" onclick="gal('.item-{{ $v->id }}')" data-target="#galleryc" class="col-sm-4 col-md-3" style="padding-left: 2px; padding-right: 2px">
                                    <div class="thumbnail cont_album_f">
                                        @if($v->file)
                                            @if($v->crop)
                                                <img src="{{ $v->crop }}" style="width: 100%;"/>
                                            @else
                                                <img src="/images/files/small/{{ $v->file }}" style="width: 100%;"/>
                                            @endif
                                        @else
                                            <img src="" alt="..." style="height: 250px">
                                        @endif
                                        <div class="captions">
                                            <div class="magnifier" >
                                                <div  >
                                                    <img src="/images/site/ic/magnifier.png"/>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
@stop