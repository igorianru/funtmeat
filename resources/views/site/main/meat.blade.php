@extends('site.layouts.default')
@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">
')

@section('title', $data[0]->name)
@section('content')
    <div class="container-fluid mainR adviceM"
         style="background-image: url('/images/files/big/{{ isset($meat[0]->file) ? $meat[0]->file : '' }}');
    background-repeat: no-repeat;
    background-position: 50% 33%;
    background-color: #a1232e;
    background-size: 100%;">
        <div class="row" style="background: rgba(0, 0, 0, .6)">
            <div class="container">
                <div class="text-mainR">
                    <div class="cont_text">
                        <ol class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            @if($data[0]['id'] == 2)
                                <li class="active">{{ $data[0]['name'] or '' }}</li>
                            @else
                                <li><a href="/meat/2">Мясо </a></li>
                                <li class="active">{{ $data[0]['name'] or '' }}</li>
                            @endif
                        </ol>
                    </div>
                    <div class="titF" style="margin-bottom: 15px; margin-top: 15px">
                        {{ $data[0]['name'] or '' }}
                    </div>
                    @if(!$page || $page == 1)
                        <div class="bm">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    {{--{!! $data[0]['text'] or '' !!}--}}
                                    <?php
                                    $i = 0;
                                    foreach ($meat as $a) {
                                    if($i < 30) {
                                    if($i == 0) { $class =  'active';} else { $class = ''; }
                                    ?>

                                    <div class="item {{ $class }}" data-image="{{ $a->file }}">
                                        <div class="carousel-caption1">
                                            <img src="/images/files/small/{{ $a->file }}" style="display: none"/>
                                            <div class="nameM">{{ $a->name }}</div>
                                            <br />

                                            <div class="col-md-12" style="text-align: justify;min-height: 300px;">
                                                <?php
                                                $stringAll = $string = strip_tags($a->text);
                                                $string = substr($string, 0, 800);
                                                $string = rtrim($string, "!,.-");
                                                $string = substr($string, 0, strrpos($string, ' '));

                                                if(strlen($stringAll) > 800) {
                                                    echo $string."…
                                                            <p style='height: 35px'>
                                                               <a style=\"text-decoration: underline;color: #fff; float: right\" href='/meat/". $a->cat ."?id=". $a->id ."'>Подробнее..</a>
                                                               </p>";
                                                } else {
                                                    echo $string;
                                                }
                                                ?>
                                            </div>
                                            <p class="text-right clear">
                                                <br />
                                            </p>
                                        </div>
                                    </div>
                                    <?php }
                                    $i++;  } ?>
                                </div>
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <?php
                                    $i = 0;
                                    foreach ($meat as $a) {
                                    if($i < 3) {
                                    if($i == 0) { $class =  'active';} else { $class = ''; }
                                    ?>
                                    <li data-target="#carousel-example-generic" data-slide-to="{{ $i }}" class="{{ $class }} aj-{{ $a->id }}"></li>
                                    <?php }
                                    $i++;  } ?>
                                </ol>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <style>
        .conMSel > div > .tit {
            font-size: 24px;
            font-weight: 600;
            color: #212121;
            text-transform: uppercase;
            margin-top: 50px;
            padding-left: 10px;
            margin-bottom: 10px;
        }
    </style>


    <div class="container" style="min-height: 100px">
        <div class="con">
            <div class="content_scr conMSel" style="min-height: 100px">
                <div class="" style="min-height: 100px">
                    <div class="tit">
                        у нас вы можете купить
                    </div>
                    @foreach($meat as $f)
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail cart cart2" onclick="$('.aj-{{ $f->id }}').click()">
                                @if($f->file)
                                    <img style="max-height: 170px;" src="/images/files/small/{{ $f->file }}">
                                @else
                                    <div class="img"></div>
                                @endif
                                <div class="caption">
                                    <p>{{ $f->name }}</p>
                                    <div class="col-md-12 text-right">
                                        <div class="row">
                                            {{--<a href="/meat/{{ $data[0]['id'] . '?id=' . $f->id }}" class="btn btn-red" role="button">Подробнее</a>--}}
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                @if(!empty($meat[0]))
                    <div class="clear text-center">
                        {!! $meat->render() !!}
                    </div>
                @endif
                <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="clear" style="border: 2px solid #bc1e2a"></div>

    <div class="container" style="min-height: 700px">
        <div class="con">
            <div class="content_scr conMSel">
                <div class="" style="min-height: 600px">
                    <div class="tit">
                        @if($data[0]['id'] == 2) рецепты блюд @endif
                        @if($data[0]['id'] == 16) рецепты блюд из говядины @endif
                        @if($data[0]['id'] == 17) рецепты блюд из свинины @endif
                        @if($data[0]['id'] == 18) рецепты блюд из баранины @endif
                        @if($data[0]['id'] == 19) рецепты блюд из птицы @endif
                        @if($data[0]['id'] == 20) рецепты блюд из дичи @endif
                    </div>
                    @foreach($recipe as $r)
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail cart">
                                <div class="rat">{{  $r->rating }}</div>
                                @if($r->file)
                                    @if($r->crop)
                                        <img style="max-height: 170px;" src="/images/files/small/{{ $r->crop }}">
                                    @else
                                        <img style="max-height: 170px;" src="/images/files/small/{{ $r->file }}">
                                    @endif
                                @else
                                    <div class="img"></div>
                                @endif
                                <div class="caption">
                                    <p>{{  $r->name }}</p>
                                    <div class="col-md-6">
                                        <div class="row tim">
                                            Время: {{  $r->time_cooking }} мин
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="row">
                                            <a href="/recipe/{{  $r->id }}" class="btn btn-red" role="button">Подробнее</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="clear"></div>
                    <div class="tit">
                        @if($data[0]['id'] == 2) интересное @endif
                        @if($data[0]['id'] == 16) интересное о говядине @endif
                        @if($data[0]['id'] == 17) интересное о свинине @endif
                        @if($data[0]['id'] == 18) интересное о баранине @endif
                        @if($data[0]['id'] == 19) интересное о птице @endif
                        @if($data[0]['id'] == 20) интересное о диче @endif
                    </div>
                    @foreach($advice as $a)
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail cart">
                                @if($a->file)
                                    @if($a->crop)
                                        <img style="max-height: 170px;" src="/images/files/small/{{ $a->crop }}">
                                    @else
                                        <img style="max-height: 170px;" src="/images/files/small/{{ $a->file }}">
                                    @endif
                                @else
                                    <div class="img"></div>
                                @endif
                                <div class="caption">
                                    <p>{{ $a->name }}</p>
                                    <div class="col-md-12 text-right">
                                        <div class="row">
                                            <a href="/meat/{{ $ids . '?id=' . $a->id }}" class="btn btn-red" role="button">Подробнее</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                {{--<div class="clear text-center">--}}
                    {{--{!! $meat->render() !!}--}}
                {{--</div>--}}
                <div class="clear"></div>
            </div>
        </div>
    </div>

    <script>
        $('.carousel').carousel({
            interval: 150000000
        }).on('slide.bs.carousel', function (e) {

            setTimeout(function () {
                var img = $('.item.active').data('image');
//            console.log('adviceM', e.delegateTarget.firstElementChild.firstElementChild.attributes['data-image'].value)

//            img = e.delegateTarget.firstElementChild.firstElementChild.attributes['data-image'].value;

                $('.adviceM').css({backgroundImage: 'url("/images/files/big/' + img + '")'});
            }, 1000)
        })
    </script>
@stop