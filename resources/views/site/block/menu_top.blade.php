<div class="container-fluid"  style="margin-bottom: 108px">
    <div class="row">
        <nav class="navbar navbar-default navbar-static-top" style="position: fixed; width: 100%">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="/images/site/logo.png" />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php
                    function view_cat($arr,$cat = 0, $i = 0)
                    {
                        if(empty($arr[$cat])) {
                            return;
                        }

                        if($cat != 0) {
                            $r = 'display:';
                        } else {
                            $r = '';
                        }

                        if(isset($arr[$cat][$i]['cat'])) {
                            $f = $arr[$cat][$i]['cat'];
                        } else {
                            $f = '';
                        }

                        if($i == 0 ) {
                            $cj = 'class="nav navbar-nav navbar-centerf"';
                        } else {
                            $cj = 'class="dropdown-menu"';
                        }

                        echo '<ul '.$cj.' >';

                        //перебираем в цикле массив и выводим на экран
                        for($i = 0; $i < count($arr[$cat]);$i++)
                        {
                            $v = $arr[$cat][$i];

                            if($v['sys_cat'])
                            {
                                $link = $v['sys_cat'];
                            } else {
                                if($v['translation'])
                                {
                                    $link = $v['controller'] . $v['translation'];
                                } else {
                                      if($v['controller'])
                                      {
                                          $link = $v['controller'] . '/' . $v['id'];
                                      } else {
                                          $link = 'page/' . $v['id'];
                                      }
                                }
                            }

                            if(empty($arr[$arr[$cat][$i]['id']])) {
                                $cj = ' ';
                            } else {
                                $cj = ' dropdown';
                            }

                            echo '<li class="li-'. $arr[$cat][$i]['id']  .$cj .'">';

                            if(empty($arr[$arr[$cat][$i]['id']])) {
                                echo '<a href="'.'/' . $link.'" class="' . $v['sys_cat'] . '">' . $arr[$cat][$i]['name'].'</a>';
                            } else {
                                echo '<a
                                        href="#"
                                        class="dropdown-toggle"
                                        data-toggle="dropdown"
                                        role="button"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                     >' . $arr[$cat][$i]['name'].' <span class="caret"></span></a>';
                            }

                            //рекурсия - проверяем нет ли дочерних категорий
                            view_cat($arr,$arr[$cat][$i]['id'], $i+1);

                            echo '</li>';

                            if(empty($arr[$arr[$cat][$i]['id']]) &&  $i+2 > count($arr[$cat]) && $v['cat'] != '') {


                                if($v['sys_cat'])
                                {
                                    $linkM = $v['sys_cat'];
                                } else {
                                    if($v['translation'])
                                    {
                                        $linkM = $v['controller'] . $v['translation'];
                                    } else {
                                        if($v['controller'])
                                        {
                                            $linkM = $v['controller'] . '/' . $v['cat'];
                                        } else {
                                            $linkM = 'page/' . $v['cat'];
                                        }
                                    }
                                }
                                if($linkM != 'actions' && $linkM != 'review')
                                {
                                    echo '
                                    <li role="separator" class="divider"></li>
                                    <li  style="text-align: center">
                                    <a href="/' .$linkM. '">Смотреть все</a>
                                    </li>';
                                }
                            }
                        }
                        echo '</ul>';
                    }

                    view_cat($menu,0, 0, 1)
                    ?>

                    <ul class="nav navbar-nav navbar-right">
                        <div class="topL">
                            <div class="t">
                                {{--+7 (443) 50-77-75--}}
                            </div>

                            <div class="b" style="padding-top: 9px">
                                <li>
                                    <button
                                            class="btn btp-callback right"
                                            data-toggle="modal"
                                            data-target="#callback"
                                    >
                                        обратный звонок
                                    </button>
                                    <br />

                                    <a href="#" style="color: #010101" data-toggle="modal" data-target="#reviewModal">
                                        Оставить отзыв
                                    </a>
                                </li>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
