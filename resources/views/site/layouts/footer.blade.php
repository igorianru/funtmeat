<!-- Modal -->
<div class="modal fade modalk" id="callback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Обратный звонок</h4>
            </div>
            <div class="modal-body text-center review-form">
                <p>
                    Закажите обратный звонок
                    - мы перезвоним Вам!
                </p>
                <br class="clear"/>
                <div class="form-group">
                    <input type="text" class="form-control name_min" name="name" placeholder="Имя"/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control phone_min" name="phone" placeholder="Телефон"/>
                </div>
                <br class="clear"/>
                <div class="form-group">
                    <button type="button" class="btn btn-main color-red" onclick="send_mail('min')">заказать обратный звонок</button>
                </div>

                <div class="mess_min"></div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade modalk" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Добавить отзыв</h4>
            </div>
            <div class="review-form">
                <p class="text-center">Добавить отзыв</p>
                <br />
                <div class="form-group">
                    <input
                            type="text"
                            class="form-control name_r"
                            placeholder="Имя"
                            aria-describedby="sizing-addon2"
                    >
                </div>
                <div class="form-group">
                    <input type="text" class="form-control phone_r" name="phone" placeholder="Телефон"/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control email_r" name="email" placeholder="E-mail"/>
                </div>
                <div class="form-group">
                    <textarea class="form-control text_r" placeholder="Текст" aria-describedby="sizing-addon2"></textarea>
                </div>
                <div class="form-group text-center">
                    <button class="btn btn-main color-red" onclick="send_rew('r')">Отправить отзыв</button>
                </div>

                <div class="mess_r"></div>
            </div>
        </div>
    </div>
</div>

<script>
    function err_inp (cl) {
        $(cl).css({boxShadow: "0px 0px 3px 2px rgb(183, 42, 31)"});
        setTimeout('$("'+cl+'").css({boxShadow: "none"})', 2500);
    }

    function send_mail(type) {
        var dataString;
        var name = $(".name_"+type).val();
        var phone = $(".phone_"+type).val();

        if(name.trim() == '') {
            err_inp(".name_"+type+"");
            return false
        }

        if(type == 'max') {
            var text = $(".text_"+type).val();

            dataString = 'type=' + type + '&name=' + name + '&phone=' + phone + '&text=' + text;
        } else {
            dataString = 'type=' + type + '&name=' + name + '&phone=' + phone + '&text=' + '';
        }

        if(phone.trim() == '') {
            err_inp(".phone_"+type+"");

            return false
        }

        $.ajax({
            cache: false,
            url: '/send_mail',
            data: dataString,
            type: 'post',
            dataType: 'JSON',
            success: function (data) {
                if (data['result'] == 'ok') {
                    $('.mess_' + type).html('<div style="margin-top: 10px" class="alert alert-success">Запрос успешно отправлен</div>').show(300);

                    setTimeout(function () {
                        $(".mess_" + type).hide(300);
                        $("#callback").modal("hide");
                    }, 2000);

                    $(".name_"+type).val('');
                    $(".phone_"+type).val('');

                    if(type == 'max') {
                        $(".email_"+type).val('');
                    }
                }
            }
        });
    }

    function send_rew(type) {
        var dataString;
        var name = $(".name_"+type).val();
        var text = $(".text_"+type).val();
        var email = $(".email_"+type).val();
        var phone = $(".phone_"+type).val();

        if(name.trim() == '') {
            err_inp(".name_"+type+"");
            return false
        }

        if(text.trim() == '') {
            err_inp(".text_"+type+"");
            return false
        }

        if(email.trim() == '') {
            err_inp(".email_"+type+"");
            return false
        }

        dataString = 'type=' + type + '&name=' + name + '&text=' + text + '&email=' + email + '&phone=' + phone;

        $.ajax({
            cache: false,
            url: '/review_add',
            data: dataString,
            type: 'post',
            dataType: 'JSON',
            success: function (data) {
                if (data['result'] == 'ok') {
                    $('.mess_' + type).html('<div style="margin-top: 10px" class="alert alert-success">' +
                            'Отзыв успешно отправлен, после модерации он появится на сайте' +
                            '</div>').show(300);

                    setTimeout(function () {
                        $(".mess_" + type).hide(300);
                        $("#reviewModal").modal("hide");
                    }, 4000);

                    $(".name_"+type).val('');
                    $(".text_"+type).val('');
                    $(".email_"+type).val('');
                    $(".phone_"+type).val('');
                }
            }
        });
    }
</script>

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-left">
                <div class="footM">
                    <div class="t">
                        Написать нам
                    </div>
                    <div class="b">
                        <a href="mailto:{{ isset($params['email']) ? $params['email']['key'] : '' }}" style="color:#fff">
                            {{ isset($params['email']) ? $params['email']['key'] : '' }}
                        </a>
                        <br />
                        <a href="/review" style="color: #fff" data-toggle="modal" data-target="#reviewModal">
                            Оставить отзыв
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                {{--<div class="footM">--}}
                    {{--<div class="t">--}}
                        {{--Режим работы--}}
                    {{--</div>--}}
                    {{--<div class="b">--}}
                        {{--Понедельник-пятница <br />--}}
                        {{--с 9:00 до 19:00--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="col-md-4 text-right">
                <div class="footM">
                    <div class="t">
                        Социальные сети
                    </div>
                    <div class="b">
                        <div class="tw" onclick="href('https://twitter.com/poundmeat')">
                            <img src="/images/site/ic/tw.png">
                        </div>
                        <div class="in" onclick="href('http://instagram.com/poundmeat')"></div>
                        <div class="ok" onclick="href('http://ok.ru/group/55200449363986 ')"></div>
                        <div class="fb" onclick="href('http://facebook.com/poundmeat')"></div>
                        <div class="vk" onclick="href('http://vk.com/poundmeat')"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@yield('footer')
<script src="/js/custom.js"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38782395 = new Ya.Metrika({
                    id:38782395,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38782395" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter38782445 = new Ya.Metrika({
                    id:38782445,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38782445" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
