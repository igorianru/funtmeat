@extends('site.layouts.default')


@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<!-- iCheck -->
<link href="' . asset('/modules/css/icheck/flat/red.css') . '" rel="stylesheet">
<!-- Switchery -->
<link href="' . asset('/modules/css/switchery/switchery.min.css') . '" rel="stylesheet">

<meta property="og:image" content="/images/files/big/' . $recipe->file . '" />
<meta property="og:title" content="' . $recipe->title . '" />
<meta property="og:description" content="' . $recipe->description . '" />
')


@section('content')
    <div class="container-fluid container-red">
        <div class="container">
            <div class="row">
                <div class="RecipeBT">
                    <div class="t">
                        <ol class="breadcrumb breadcrumbFFF">
                            <li><a href="/">Главная</a></li>
                            <li><a href="/catalog">Рецепты</a></li>
                            <li class="active">
                                {{ $recipe->name }}
                            </li>
                        </ol>
                    </div>
                    <div class="c shGh-div">
                        <div class="col-md-12">
                            <div class="nameM">{{ $recipe->name }}</div>
                            <div class="textM text-justify">
                                {{ $recipe->little_description }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="text-main thumbnail">
                                <img src="/images/files/big/{{ $recipe->file }}">
                            </div>
                        </div>

                        <div class="col-md-6 text-right">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="rating-main rating-recipe">
                                        <div>
                                            <div class="t">
                                                {{ $recipe->rating }}
                                            </div>
                                            <div class="b">
                                                рейтинг
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-left">
                                    <div class="footM recipeM">
                                        <div class="t">
                                            Поделитесь рецептом!
                                        </div>
                                        <div class="b">
                                            {{--<div class="in"></div>--}}
                                            <div class="ok" onclick="Share.odnoklassniki('' + window.location.href + '', '{{ htmlspecialchars($recipe->little_description) }}')"></div>
                                            <div class="fb" onclick="Share.facebook('' + window.location.href + '','{{ $recipe->title }}','{{ 'http://'. $_SERVER['HTTP_HOST']. '/images/files/big/'.  $recipe->file }}', '{{ htmlspecialchars($recipe->little_description) }}')"></div>
                                            <div onclick="Share.vkontakte('' + window.location.href + '','{{ $recipe->title }}','{{ 'http://'. $_SERVER['HTTP_HOST']. '/images/files/big/'.  $recipe->file }}', '{{ htmlspecialchars($recipe->little_description) }}')" class="vk"></div>
                                        </div>

                                    </div>
                                    <div class="progress-main progress-recipe">
                                        <div>
                                            <div class="l">
                                                Сложность
                                            </div>
                                            <div class="r">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success"
                                                         role="progressbar"
                                                         aria-valuenow="{{ $recipe->simple_recipe }}"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100"
                                                         style="width: {{ $recipe->simple_recipe }}%"
                                                    >
                                                        <span class="sr-only">{{ $recipe->simple_recipe }}% Complete (success)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="progress-main progress-recipe">
                                        <div>
                                            <div class="l">
                                                Время
                                            </div>
                                            <div class="r">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success"
                                                         role="progressbar"
                                                         aria-valuenow="{{ $recipe->time_cooking }}"
                                                         aria-valuemin="0"
                                                         aria-valuemax="100"
                                                         style="width: {{ $recipe->time_cooking/(180/100) }}%"
                                                    >
                                                <span class="sr-only">
                                                    {{ $recipe->time_cooking/(180/100) }}% Complete (success)
                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 text-left">
                                <div class="row">
                                    <div class="typeText">
                                        <div class="t">
                                            @if($recipe->type_cooking == 1)
                                                <div class="ic t-1"></div>
                                                <span>Жарка</span>
                                            @endif
                                            @if($recipe->type_cooking == 2)
                                                <div class="ic t-2"></div>
                                                <span>Запекание</span>
                                            @endif
                                            @if($recipe->type_cooking == 3)
                                                <div class="ic t-3"></div>
                                                <span>Тушение</span>
                                            @endif
                                            @if($recipe->type_cooking == 4)
                                                <div class="ic t-4"></div>
                                                <span>Варка</span>
                                            @endif
                                            @if($recipe->type_cooking == 5)
                                                <div class="ic t-5"></div>
                                                <span>Запекание</span>
                                            @endif
                                            @if($recipe->type_cooking == 6)
                                                <div class="ic t-6"></div>
                                                <span>Гриль / барбекю</span>
                                            @endif
                                            @if($recipe->type_cooking == 7)
                                                <div class="ic t-7"></div>
                                                <span>Соусы</span>
                                            @endif
                                        </div>
                                        <div class="c">
                                            @if($recipe->time_cooking <= 30)
                                                <div class="ic t-1"></div>
                                                <span>Менее 30 минут</span>
                                            @endif
                                            @if($recipe->time_cooking >= 30 && $recipe->time_cooking <= 60)
                                                <div class="ic t-2"></div>
                                                <span>От 30 минут до часа</span>
                                            @endif
                                            @if($recipe->time_cooking > 60)
                                                <div class="ic t-3"></div>
                                                <span>Более часа</span>
                                            @endif
                                        </div>
                                        <div class="b">
                                            @if($recipe->type_meat == 1)
                                                <div class="ic t-1"></div>
                                                <span>Говядина</span>
                                            @endif
                                            @if($recipe->type_meat == 2)
                                                <div class="ic t-2"></div>
                                                <span>Свинина</span>
                                            @endif
                                            @if($recipe->type_meat == 3)
                                                <div class="ic t-3"></div>
                                                <span>Баранина</span>
                                            @endif
                                            @if($recipe->type_meat == 4)
                                                <div class="ic t-4"></div>
                                                <span>Птица</span>
                                            @endif
                                            @if($recipe->type_meats == 5)
                                                <div class="ic t-5"></div>
                                                <span>Дичь</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="titRev">Пожалуйста, оцените этот рецепт:</div>
                                    <div class="reviewStars-input reviewStars">
                                        <input
                                            @if(round($recipe->rating) == 10) checked @endif
                                            id="star-10"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-10"
                                        />
                                        <label title="10" for="star-10" class="vote star-10"></label>

                                        <input
                                            @if(round($recipe->rating) == 9) checked @endif
                                            id="star-9"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-9"
                                        />
                                        <label title="9" for="star-9" class="vote star-9"></label>

                                        <input
                                            @if(round($recipe->rating) == 8) checked @endif
                                            id="star-8"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-8"
                                        />
                                        <label title="8" for="star-8" class="vote star-8"></label>

                                        <input
                                            @if(round($recipe->rating) == 7) checked @endif
                                            id="star-7"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-7"
                                        />
                                        <label title="7" for="star-7" class="vote star-7"></label>

                                        <input
                                            @if(round($recipe->rating) == 6) checked @endif
                                            id="star-6"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-6"
                                        />
                                        <label title="6" for="star-6" class="vote star-6"></label>

                                        <input
                                            @if(round($recipe->rating) == 5) checked @endif
                                            id="star-5"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-5"
                                        />
                                        <label title="5" for="star-5" class="vote star-5"></label>

                                        <input
                                            @if(round($recipe->rating) == 4) checked @endif
                                            id="star-4"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-4"
                                        />
                                        <label title="4" for="star-4" class="vote star-4"></label>

                                        <input
                                            @if(round($recipe->rating) == 3) checked @endif
                                            id="star-3"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-3"
                                        />
                                        <label title="3" for="star-3" class="vote star-3"></label>

                                        <input
                                            @if(round($recipe->rating) == 2) checked @endif
                                            id="star-2"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-2"
                                        />
                                        <label title="2" for="star-2" class="vote star-2"></label>

                                        <input
                                            @if(round($recipe->rating) == 1) checked @endif
                                            id="star-1"
                                            type="radio"
                                            name="reviewStars"
                                            autocomplete="off"
                                            class="voteInp voteInp-1"
                                        />
                                        <label title="1" for="star-1" class="vote star-1"></label>
                                    </div>
                                </div>


                                <script>
                                    $(document).ready(function () {
                                        $('.vote').click(function () {
                                            var request = {
                                                id_recipe: '{{ $recipe->id }}',
                                                rating: $(this).attr('title')
                                            };

                                            $.ajax
                                            ({
                                                type: "get",
                                                url: "/rating",
                                                data: request,
                                                cache: false,
                                                dataType: "JSON",
                                                success: function (data) {
                                                    if (data['result'] == 'ok') {
                                                        if(data.rating != 'error')
                                                        {
                                                            $('.rating-main > div > .t').html(data.rating_des);
                                                            $('.voteInp').removeAttr('checked');
                                                            $('.voteInp-' + data.rating).attr('checked', '');
                                                            $('.voteInp').attr('disabled', 'disabled');
                                                        } else {
                                                            data.rating = parseInt($('.rating-main > div > .t').html());
                                                        }

                                                        $('.vote').removeClass('active');
                                                        for(var i = 1; data.rating >= i; i++) {
                                                            $('.star-' + i).addClass('active');
                                                        }
                                                    }
                                                }
                                            });
                                        })
                                    })
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid ">
        <div class="container">
            <div class="row">
                <div class="col-md-12 b ">
                    <div class="shGh-div" style="margin-bottom: 20px">
                        <div style="   overflow: hidden;">
                            {!! $recipe->text !!}
                        </div>
                        <div class="titAll text-right">
                            Приятного аппетита!
                        </div>
                    </div>
                </div>
                <div class="clear"></div>

                <div class="selResult">
                    @if(!empty($similar[0]))
                        <div class="titAllSlid text-left">
                            Похожие рецепты
                        </div>
                    @endif
                    <div class="clear"></div>
                    @foreach($similar as $a)
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail cart">
                                @if($a->file)
                                    @if($a->crop)
                                        <img style="max-height: 170px;" src="/images/files/small/{{ $a->crop }}">
                                    @else
                                        <img style="max-height: 170px;" src="/images/files/small/{{ $a->file }}">
                                    @endif
                                @else
                                    <div class="img"></div>
                                @endif
                                <div class="caption">
                                    <p>{{ $a->name }}</p>
                                    <div class="col-md-12 text-right">
                                        <div class="row">
                                            <a href="/recipe/{{ $a->id }}" class="btn btn-red" role="button">Подробне</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@section('footer', '
<!-- share -->
<script src="/js/share.js"></script>
<!-- lodash -->
<script src="/js/lodash.min.js"></script>
<!-- iCheck -->
<script src="' . asset('/modules/js/icheck/icheck.min.js') . '"></script>
<!-- Switchery -->
<script src="' . asset('/modules/js/switchery/switchery.min.js') . '"></script>
')
@stop
