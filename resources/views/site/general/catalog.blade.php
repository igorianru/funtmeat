@extends('site.layouts.default')


@section('header', '<link href="/css/slider_main/advanced-slider-base.css" rel="stylesheet">
<script src="/js/bootstrap/js/slider_main/jquery.touchSwipe.min.js" charset="UTF-8"></script>
<script src="/js/bootstrap/js/slider_main/jquery.advancedSlider.min.js" charset="UTF-8"></script>
<script src="/js/scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<link href="/js/scrollbar/jquery.scrollbar.css" rel="stylesheet">

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<!-- iCheck -->
<link href="' . asset('/modules/css/icheck/flat/red.css') . '" rel="stylesheet">
<!-- Switchery -->
<link href="' . asset('/modules/css/switchery/switchery.min.css') . '" rel="stylesheet">
')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row conASel">
                    <div class="col-md-3 l">
                        <ol class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active">
                                {{ isset($data[0]->name) ? $data[0]->name : '' }}
                            </li>
                        </ol>
                        <div class="conLSel typeCooking">
                            <div class="tit">Способ приготовления</div>
                            <div class="b">
                                <div data-type-cooking="0" class="active">Любой</div>
                                <div data-type-cooking="1">Жарка</div>
                                <div data-type-cooking="2">Запекание</div>
                                <div data-type-cooking="3">Тушение</div>
                                <div data-type-cooking="4">Варка</div>
                                <div data-type-cooking="5">Гриль / барбекю</div>
                                <div data-type-cooking="6">Соусы</div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="conLSel timeCooking">
                            <div class="tit">Время приготовления</div>
                            <div class="b">
                                <div data-time-cooking="0" class="active">Любое</div>
                                <div data-time-cooking="1">Менее 30 минут</div>
                                <div data-time-cooking="2">От 30 минут до часа</div>
                                <div data-time-cooking="3">Более часа</div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="conLSel typeMeat">
                            <div class="tit">Тип мяса</div>
                            <div class="b">
                                <div data-type-meat="0" class="active">Любое</div>
                                <div data-type-meat="1">Говядина</div>
                                <div data-type-meat="2">Свинина</div>
                                <div data-type-meat="3">Баранина</div>
                                <div data-type-meat="4">Птица</div>
                                <div data-type-meat="5">Дичь</div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="conLSel">
                            <div class="tit">Параметры</div>
                            <div class="b">
                                <label>Простой рецепт</label>
                                <input type="checkbox" class="flat" name="simple_recipe" value="1"/>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                     <div class="col-md-9 r">
                        <div class="conRSel">
                            <div class="tit">
                                {{ isset($data[0]->title) ? $data[0]->title : '' }}
                            </div>
                            <div class="b">
                               {!! isset($data[0]->text) ? $data[0]->text : '' !!}
                            </div>
                            <div class="selReN">Найдено: <span class="i">0</span> <span>рецептов</span></div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="filterGroup">
                                        <div data-filter-group="10">Сначала новые</div>
                                        <div data-filter-group="20">По времени приготовления</div>
                                        <div data-filter-group="30">По рейтингу</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="row">
                                    <div class="input-group searchSel">
                                        <input
                                                aria-describedby="inputSuccess2Status"
                                                type="text"
                                                class="form-control inputSearch"
                                                name="input_search"
                                                placeholder="Поиск"
                                                onkeypress="return filCat.searchGo(event)"
                                        />
                                        <span
                                                class="glyphicon glyphicon-search form-control-feedback"
                                                aria-hidden="true"
                                        ></span>
                                    </div><!-- /input-group -->
                                </div>
                            </div>
                            <div class="clear"></div>
                            <hr class="clear hrSel"/>
                        </div>
                        <div class="selResult"></div>

                         <input type="hidden" class="pagination" value="{{ $page }}">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="/js/filters-categories.js"></script>
    <script>
        $(document).ready(function(){

            filCat.initialize({
                url_req : '/',
                num : '.selReN > .i',
                cont : '.selResult'
            });

            setTimeout(function () {
                filCat.selectCatalogs();
            }, 500)
        });

    </script>

@section('footer', '
<!-- lodash -->
<script src="/js/lodash.min.js"></script>
<!-- iCheck -->
<script src="' . asset('/modules/js/icheck/icheck.min.js') . '"></script>
<!-- Switchery -->
<script src="' . asset('/modules/js/switchery/switchery.min.js') . '"></script>
')
@stop