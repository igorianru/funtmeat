@extends('shop.layouts.default')
@section('content')
    <!-- content -->
    <div class="container-fluid fon_wood">
        <div class="container">
            <div class="row breadmargin">
                <ol class="breadcrumb">
                    <li><a href="/shop">Главная</a></li>
                    <li><a href="/shop/categories">Каталог</a></li>
                    <li><a href="#">Говядина</a></li>
                </ol>
            </div>

            <div class="aboutcompany"><h2>Говядина</h2></div>

            <div class="cont">
                <div class="product-{{ $product['id'] }}">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><b>{{ $product['name'] }}</b></h3>
                            </div>
                            <div class="col-md-4">
                                <div class="products">
                                    <div class="thumbnail">
                                        @if($product['file'])
                                            @if($product['crop'])
                                                <img src="/images/files/small/{{ $product['crop'] }}" />
                                            @else
                                                <img src="/images/files/small/{{ $product['file'] }}" />
                                            @endif
                                        @else
                                            <img src="/images/files/small/no_img.png" class="blur1" style="width: 100%" />
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                {!! $product['text'] !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="red_price">
                            @if(!$product['not_available'])
                                <p class="price-money">
                                    <?php $price = $product['price_money'] - ($product['price_money']/100 * $product['discount']) ?>
                                    <span data-price-kilogram="{{ $price }}">{{ $price }}</span>

                                    <i class="glyphicon glyphicon-ruble"></i>
                                </p>

                                <div>
                                    <button type="button" class="btn pluss" onclick="filCat.poundWeight('{{ $product['id'] }}', 'minus')">
                                        <i class="glyphicon glyphicon-minus"></i>
                                    </button>

                                    <span data-type-measure="{{ $product['type_measure'] }}">
                                        {!! [1, 1.0, 1.0, 1, 1][$product['type_measure']] ?? '' !!}
                                    </span>

                                    {!! ['шт', 'кг', 'л', 'шт', 'шт'][$product['type_measure']] ?? '' !!}

                                    <button type="button" class="btn pluss" onclick="filCat.poundWeight('{{ $product['id'] }}', 'plus')">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </button>
                                </div>
                                <button type="button" class="btn btn-default white_button" onclick="filCat.addToCart('{{ $product['id'] }}', 'add')">
                                    В корзину
                                </button>
                            @else
                                <button type="button" class="btn btn-default white_button" disabled="disabled">
                                    Нет в наличии
                                </button>
                            @endif
                        </div>
                    </div>

                    @if(!empty($product['information']))
                        <div class="row"><hr /></div>

                        <div class="col-md-12">
                            <h4><b>Информация</b></h4>
                            {!! $product['information'] !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid fon_wood">
        <div class="container">
            <div class="cont ">
                <p class="title-cont">С этим товаром часто покупают:</p>
                @include('shop.main.block_main.product_sell_out', $data['products_sell_out'] = $products_sell_out)
            </div>
        </div>
    </div>
    <!-- content -->
@stop
