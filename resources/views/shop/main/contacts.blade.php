@extends('shop.layouts.default')

@section('header', '
<script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>')

@section('content')
    <style>
        #map {
            height: 400px;
            width: 100%;
            position: absolute;
            z-index: 1;
        }

        .slide-div {
            height: 410px;
        }
    </style>

    <!-- content -->
    <div class="container-fluid fon_wood">
        <div class="container">
            <div class="row breadmargin">
                <ol class="breadcrumb">
                    <li><a href="/shop">Главная</a></li>
                    <li><a href="#">Контакты</a></li>
                </ol>
            </div>
            <div class="aboutcompany"><h2>Контакты</h2></div>

            <div class="cont" style="min-height: 500px; display: block; position: relative; overflow: hidden">
                {!! $page['html_top'] !!}
                <div class="">{!! $page['text'] !!}</div>
                {!! $page['html_bottom'] !!}
            </div>
        </div>
    </div>
    <!-- content -->
@stop
