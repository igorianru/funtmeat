@extends('shop.layouts.default')
@section('content')
<!-- content -->
<div class="container-fluid fon_wood" style="position: relative;">
    <div class="main_left_m"></div>
    <div class="main_right_m"></div>

    <div class="header_block">
        <div class="container" style="z-index: 1; position: relative">
            <p class="text-uppercase a"> интернет - магазин</p>
            <p class="text-uppercase b"> Фунт <span>мяса</span></p>
            <p class="c">Всегда хороший выбор хорошего мяса.</p>
            <a href="/shop/categories" type="button" class="btn btn-danger d text-uppercase">продукция</a>

            <div class="main_top_m"></div>
        </div>
    </div>

    <div class="container" style="z-index: 1; position: relative">
        <!--Product categories-->
        <div class="cont1">
            <div class="col-md-12">
                <div class="Product_categories row">
                    <div class="col-sm-8 col-md-9 col-lg-10 a">
                        <p class="text-uppercase "> Категория <span>Продуктов</span></p>
                    </div>
                    <div class="col-sm-4 col-md-3 col-lg-2 b text-right">
                        <button type="button" class="btn btn-danger d">Все категории</button>
                    </div>
                </div>
            </div>

            @php($i = 0)
            @foreach($shop_categories as $val)
                @if($i < 6)
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-2 img-responsive" >
                        <a href="/shop/categories/{{ $val['id'] }}" class="decoration-none">
                            <div class="img-categories">
                                <div class="t thumbnail">
                                    @if($val['file'])
                                        @if($val['crop'])
                                            <img src="/images/files/small/{{ $val['crop'] }}">
                                        @else
                                            <img src="/images/files/small/{{ $val['file'] }}">
                                        @endif
                                    @else
                                        <img src="/images/shop/devimg.png">
                                    @endif

                                    <div class="f"></div>
                                </div>

                                <div class="b">
                                    <button type="button" class="btn btn-danger">
                                        Все категории
                                    </button>
                                </div>
                                <p class="text-uppercase">{{ $val['name'] }}</p>
                            </div>
                        </a>
                    </div>
                    @php($i++)
                @endif
            @endforeach
        </div>

        <!--Special offers-->
        <div class="col-md-12 grey-fon">
            <div class="Product_categories">
                <div class="col-md-10 col-sm-9 a">
                    <p class="text-uppercase"> наша <span>компания</span></p>
                </div>
                <div class="col-md-2 b text-right">
                    <a type="button" class="btn btn-danger d" href="/shop/actions">Все акции</a>
                </div>
            </div>
        </div>
        <!--our company-->
        <div class="cont1 ">
            @foreach($actions as $k => $v)
                <div class="our_company {!! $k === 0 ? '' : 'hide' !!}" data-k="{{ $k + 1 }}">
                    <div class="col-lg-6 a">
                        {!! $v['little_description'] !!}
                        <div class="c">
                            @foreach($actions as $k => $vv)
                                <a class="btn btn-default" href="javascript:void(0)" role="button" onclick="showN({{ $k + 1 }})">
                                    {{ $k + 1 }}
                                </a>
                            @endforeach
                        </div>
                    </div>

                    <div class="col-lg-6 b">
                        <div onclick="href('/shop/actions/{{ $v['id'] }}')" style="cursor: pointer">
                            @if($v['file'])
                                @if($v['crop'])
                                    <img src="/images/files/small/{{ $v['crop'] }}">
                                @else
                                    <img src="/images/files/small/{{ $v['file'] }}">
                                @endif

                            @else
                                <img src="/images/files/no-image.jpg">
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach

            <script>
				function showN(n) {
					$('.our_company').addClass('hide');
					$('[data-k="' + n + '"]').removeClass('hide');
				}
            </script>
        </div>
        <!--our company-->

        <!--Product categories-->
        <div class="col-md-12 grey-fon">
            <div class="Product_categories">
                <div class="col-md-10 col-sm-9 a">
                    <p class="text-uppercase">популярные <span>Продукты</span></p>
                </div>
                <div class="col-md-2 col-sm-3 b text-right">
                    <button onclick="href('/shop/categories')" type="button" class="btn btn-danger d">
                        Вся продукция
                    </button>
                </div>
            </div>
        </div>
        <!--Popular products-->

        <div class="cont1">
            <div class="selResult">
                <p><img style="padding-top: 20px" src="/images/shop/logo.png" class="bgCategories"/></p>
            </div>

            <input type="hidden" class="pagination" value="0">
            <input type="hidden" name="category" value="-1">
        </div>

        <!--Popular products-->
        <div class="col-md-12 grey-fon">
            <div class="Product_categories">
                <div class="col-md-10 col-sm-9 a">
                    <p class="text-uppercase "> специальные <span>Предложения</span></p>
                </div>
                <div class="col-md-2 b text-right">
                    <a type="button" class="btn btn-danger d" href="/shop/categories?actions=true">Все акции</a>
                </div>
            </div>
        </div>
        <!--Special offers-->

        <div class="cont1 cont1-margin-bottom">
            <div class="special_offers">
                <div class="col-md-7 a">
                    <div class="offers">
                        <div class="col-md-5 b">
                            <p>Шашлык свиной <br> <del class="d">960 р/кг</del> <br><span class="e">960 р/кг</span></p>
                        </div>

                        <div class="col-md-12"><button type="button" class="btn btn-danger">В корзину</button></div>
                        <img class="big" src="/images/shop/1.jpg" />
                    </div>
                </div>

                <div class="col-md-5 a" >
                    <div class="offers ">
                        <div class="col-md-11 b">
                            <p>
                                Шашлык свиной <br> <span class="d"> скелетная поперечно-полосатая мускулатура животного
                                с прилегающими к ней жировой и соединительной тканями, а также прилегающей</span>
                            </p>
                        </div>

                        <div class="col-md-12"><button type="button" class="btn btn-danger">В корзину</button></div>
                        <img class="small" src="/images/shop/3.png">
                    </div>
                </div>

                <div class="col-md-5 a">
                    <div class="offers">
                        <div class="col-md-11 b">
                            <p>Шашлык свиной <br> <del class="d">960 р/кг</del> <br><span class="e">960 р/кг</span></p>
                        </div>

                        <div class="col-md-12"><button type="button" class="btn btn-danger">В корзину</button></div>
                        <img class="small" src="/images/shop/3.png">
                    </div>
                </div>

                <div class="col-md-7 a">
                    <div class="offers">
                        <div class="col-md-6 b">
                            <p>
                                Шашлык свиной <br> <span class="d">2 кг по цене 1 кг.</span >
                                <br />
                                <span class="e">670 p</span>
                            </p>
                        </div>

                        <div class="col-md-12"><button type="button" class="btn btn-danger">В корзину</button></div>
                        <img class="big" src="/images/shop/2.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer_m"></div>
</div>
<!-- content -->
@section('footer_cart', '
    <script>
        $(document).ready(function(){

            filCat.initialize({
                url_req   : \'/\',
                num       : \'.selReN > .i\',
                cont      : \'.selResult\',
                count_box : \'8\',
                class_box : \'col-xs-12 col-sm-6 col-md-4 col-lg-3\',
                pagination: false,
                order_by  : \'popular_product\',
            });

            setTimeout(function () {
                filCat.selectCatalogs();
            }, 500)
        });
    </script>
')
@stop