@extends('shop.layouts.default')
@section('content')
    <!-- content -->
    <div class="container-fluid fon_wood">
        <div class="container">
            <div class="row breadmargin">
                <ol class="breadcrumb">
                    <li><a href="/shop">Главная</a></li>
                    <li><a href="#">Корзина</a></li>
                </ol>
            </div>
            <div class="aboutcompany">
                <h2>Корзина</h2>
            </div>
            <div>
                <div class="col-md-8">
                    <div class="cont row-left" style="min-height: 350px">
                        <div class="basketCont">
                            <?php $sum = 0 ;?>
                            @foreach($product as $val)
                                <?php $weights = $cart[$val['id']]['weights'] ?? 0 ?>
                                <div class="shopping-basket row product-cart-shop-{{ $val['id'] }} product-{{ $val['id'] }}">
                                    <div class="col-md-3 col-sm-6 i">
                                        <a href="javascript:void(0)" onclick="filCat.addToCart('{{ $val['id'] }}', 'remove')">
                                            <i class="glyphicon glyphicon-remove"></i>
                                        </a>

                                        <a href="/product/{{ $val['id'] }}">
                                            @if(!$val['file'])
                                                <img src="/images/files/small/no_img.png" style="width: 65px; height: 65px"/>
                                            @else
                                                @if($val['crop'])
                                                    <img src="/images/files/small/{{ $val['crop'] }}" style="width: 65px; height: 65px"/>
                                                @else
                                                    <img src="/images/files/small/{{ $val['file'] }}" style="width: 65px; height: 65px"/>
                                                @endif
                                            @endif

                                            @if($val['discount'])<div class="discount">{{ $val['discount'] }}%</div>@endif
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-sm-6 t">
                                        <p>{{ $val['name'] }}</p>
                                    </div>
                                    <div class="col-md-3 col-sm-6 c">
                                        <button type="button" class="btn btn-weight min" onclick="filCat.poundWeight('{{ $val['id'] }}', 'minus', true)">
                                            <i class="glyphicon glyphicon-minus"></i>
                                        </button>

                                        <span class="weight">
                                            <span>{{ $weights/1000 }}</span>
                                            {!! $type_measure[$val['type_measure']] !!}
                                        </span>

                                        <button type="button" class="btn btn-weight min" onclick="filCat.poundWeight('{{ $val['id'] }}', 'plus', true)">
                                            <i class="glyphicon glyphicon-plus"></i>
                                        </button>
                                    </div>

                                    <div class="col-md-3 col-sm-6 p text-right">
                                        <?
                                        $price = ($val['price_money'] - ($val['price_money']/100 * $val['discount'])) / 1000 * $weights;
                                        $sum = $sum + $price;
                                        ?>
                                        <span>
                                        {{ ($val['price_money'] - ($val['price_money']/100 * $val['discount'])) / 1000 * $weights }}
                                            <i class="glyphicon glyphicon-ruble"></i>
                                    </span>

                                        @if($val['discount'])
                                            <s>
                                                {{ $val['price_money'] / 1000 * $weights }}
                                                <i class=" glyphicon glyphicon-ruble"></i>
                                            </s>
                                        @endif
                                    </div>
                                </div>
                            @endforeach

                        </div>

                        <div class="text-right">
                            <br />
                            <a href="/shop/categories" class="btn btn-danger">Вернуться к покупкам</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="cont_ba">
                            <div class="delivery">
                                <div class="col-md-8">
                                    <span>Доставка:</span>

                                    <div class="time-current-ordering-address" style="margin-left: 10px; margin-bottom: -10px">
                                        {{ $address }}
                                    </div>

                                    <div class="money1 time-current-ordering">
                                        <?php
                                        $t = explode('-', $time_current)
                                        ?>
                                        {!! count($t) == 3 ?
                                         (int) $time_current_day . ' '. $time_current_month . ', ' .
                                         'с ' . $t[0] . ' до ' . $t[1] : 'время не выбрано'
                                         !!}
                                    </div>
                                </div>

                                <div class="col-md-4 ">
                                    <button
                                        type="button"
                                        class="btn btn-danger"
                                        data-toggle="modal"
                                        data-target=".timeModal"
                                    >Сменить</button>
                                </div>

                                <div class="col-md-12">
                                    <a href="/shop/contacts#termsOfDelivery" style="color: #0b0b0b; text-decoration: underline">
                                        Условия доставки
                                    </a>
                                </div>
                            </div>

                            <div class="fix">
                                <div class="order_price col-md-12">
                                    <div class="row">
                                        <div class="col-md-12 p">
                                            <p class="money1 ">Сумма заказа:</p>
                                            <span class="moneyTop">{{ $sum }}</span>
                                            <i class="glyphicon glyphicon-ruble"></i>
                                        </div>

                                        <div class="col-md-12 text-right">
                                            <button type="button" onclick="href('/shop/ordering')" class="btn btn-danger">
                                                Оформить заказ
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cont ">
                    <p class="title-cont">С продуктами из вашей карзины хорошо сочетаются:</p>
                    @include('shop.main.block_main.product_sell_out', $data['products_sell_out'] = $products_sell_out)
                </div>
            </div>
        </div>
    </div>
    <!-- content -->
@stop
