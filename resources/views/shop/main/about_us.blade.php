@extends('shop.layouts.default')
@section('content')
<!-- content -->
<div class="container-fluid fon_wood">
    <div class="container">

        <div class="row breadmargin">
            <ol class="breadcrumb">
                <li><a href="/shop">Главная</a></li>
                <li><a href="#">{{ $page[0]['name'] or '' }}</a></li>
            </ol>
        </div>
        <div class="aboutcompany">
            <h2>{{ $page[0]['name'] or '' }}</h2>
        </div>
        <div class="cont" style="overflow: hidden;">
            {!!  $page[0]['text'] or '' !!}
        </div>
    </div>
</div>
<!-- content -->
@stop
