@extends('shop.layouts.default')
@section('content')
    <!-- content -->
    <div class="container-fluid fon_wood">
        <div class="container">
            <div class="row breadmargin" id="breadmargin">
                <ol class="breadcrumb">
                    <li><a href="/shop">Главная</a></li>
                    <li><a href="/shop/categories/">Каталог</a></li>

                    @if(($parent_category[0]['id'] ?? false) && ($parent_category[0]['cat'] ?? true))
                        <li>
                            <a href="/shop/categories/{{ $parent_category[0]['id'] }}">
                                {{ $parent_category[0]['name'] }}
                            </a>
                        </li>
                    @endif

                    @if($category > 0)<li><a href="javascript:void(0)">{{ $meta_c['name'] }}</a></li>@endif
                </ol>
            </div>

            <div class="aboutcompany"><h2>{{ $meta_c['name'] ?? 'Все категории' }}</h2></div>

            <div class="row">
                <div class="col-md-2 col-md-25">
                    <div class="cont">
                        <ul class="meat_catalog" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php $i = 0 ?>
                            @foreach($menu[15] as $val)
                                <li class="{!! $category == $val['id'] ? 'active' : '' !!}">
                                    <a href="/shop/categories/{{ $val['id'] }}#breadmargin">{{ $val['name'] }}</a>

                                    <ul
                                        class="collapse {!! $category == $val['id'] ? 'in' : '' !!}
                                        @foreach($menu[$val['id']] ?? [] as $v0)
                                        {!! $v0['id'] == $category ? 'in' : '' !!}@endforeach"

                                        role="tabpanel"
                                        aria-labelledby="headingOne"
                                    >
                                        @foreach($menu[$val['id']] ?? [] as $v)
                                            <li class="{!! $category == $v['id'] ? 'active' : '' !!}">
                                                <a href="/shop/categories/{{ $v['id'] }}#breadmargin">
                                                    {{ $v['name'] }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                                <?php $i++ ?>
                            @endforeach
                        </ul>

                        <div class="conLSel">
                            <div class="tit" style="margin:15px 0"><h4><b>Параметры</b></h4></div>
                            <div class="b" style="text-align: right; height: 25px;">
                                <label style="float: left;">Акции</label>

                                <input
                                    type="checkbox"
                                    autocomplete="off"
                                    class="flat"
                                    {!! isset($_REQUEST['actions']) ? 'checked' : '' !!}
                                    name="actions"
                                    value="1"
                                />
                            </div>
                            <div class="clear"></div>

                            <div class="b" style="text-align: right; height: 25px;">
                                <label style="float: left;">Сезонные товары</label>

                                <input
                                    type="checkbox"
                                    autocomplete="off"
                                    class="flat"
                                    {!! isset($_REQUEST['seasonal_goods']) ? 'checked' : '' !!}
                                    name="seasonal_goods"
                                    value="1"
                                />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-9 col-md-85">
                    <div class="cont">
                        <div  class="col-md-9">
                            <div class="input-group">
                                <input style="height: 32px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;" value="{{ $_GET['s'] ?? '' }}" type="text" name="input_search" class="form-control" placeholder="Поиск" />

                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger" onclick="filCat.selectCatalogs();">
                                        Поиск
                                    </button>
                                </span>
                            </div>
                        </div>

                        <div  class="col-md-3">
                            <div class="dropdown btn_popular">
                                <a class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <u> По популрности</u>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="selResult">
                            <p><img style="padding-top: 20px" src="/images/shop/logo.png" class="bgCategories"/></p>
                        </div>
                        <input type="hidden" class="pagination" value="{{ $page }}">
                        <input type="hidden" name="category" value="{{ $category }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content -->
@section('footer_cart', '
    <script>
        $(document).ready(function(){

            filCat.initialize({
                cont      : \'.selResult\',
                num       : \'.selReN > .i\',
                pagination: true,
                url_req   : \'/\',
            });

            setTimeout(function () {
                filCat.selectCatalogs();
            }, 500)
        });
    </script>
')
@stop