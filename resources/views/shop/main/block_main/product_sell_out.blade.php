@foreach($products_sell_out as $val)
    <div class="col-md-3 product-{{ $val['id'] }}">
        <div class="shop-cart-big">
            <div>
                <div class="t thumbnail">
                    <a href="/shop/product/{{ $val['id'] }}">
                        @if($val['file'])
                            @if($val['crop'])
                                <img src="/images/files/small/{{ $val['crop'] }}" />
                            @else
                                <img src="/images/files/small/{{ $val['file'] }}" />
                            @endif
                        @else
                            <img src="/images/files/small/no_img.png" class="blur1" style="width: 100%" />
                        @endif
                    </a>
                </div>
                <div class="b">
                    <div class="z">{{ $val['name'] }}</div>
                    <div class="m">
                        <button type="button" class="btn" onclick="filCat.poundWeight('{{ $val['id'] }}', 'minus')">
                            <i class="glyphicon glyphicon-minus"></i>
                        </button>

                        <span class="weight">
                            <span data-type-measure="{{ $val['type_measure'] }}">
                                {!! [1, 1.0, 1.0, 1, 1][$val['type_measure']] ?? '' !!}
                            </span>

                            {!! ['шт', 'кг', 'л', 'шт', 'шт'][$val['type_measure']] ?? '' !!}
                        </span>

                        <button type="button" class="btn" onclick="filCat.poundWeight('{{ $val['id'] }}', 'plus')">
                            <i class="glyphicon glyphicon-plus"></i>
                        </button>

                        <div class="p">
                            <div class="col-md-6 text-left">
                                <div class="row price-money">
                                    <?php $price = $val['price_money'] - ($val['price_money']/100 * $val['discount']) ?>
                                    <span data-price-kilogram="{{ $price }}">{{ $price }}</span>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="row">
                                    <a href="javascript:void(0)" onclick="filCat.addToCart('{{ $val['id'] }}', 'add')">
                                        <img src="/images/shop/shopping_cart.png">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach