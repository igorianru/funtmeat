@extends('shop.layouts.default')
@section('content')
    <!-- content -->
    <div class="container-fluid fon_wood">
        <div class="container">
            <div class="row breadmargin">
                <ol class="breadcrumb">
                    <li><a href="/shop">Главная</a></li>
                    <li><a href="/shop/basket">Корзина</a></li>
                    <li><a href="#">Оформление заказа</a></li>
                </ol>
            </div>
            <div class="aboutcompany">
                <h2>Оформление заказа:</h2>
            </div>
            <div class="cont cont-ordering">
                <div class="col-md-12">
                    <div class="order_data">
                        <h3>Данные заказа</h3>
                        <p> Сумма заказа: </p>
                        <span><span class="moneyTop" id="moneyTopId"></span> <i class="glyphicon glyphicon-ruble"></i></span>
                    </div>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div>
                    <div class="col-md-5">
                        <div class="delivery_address">
                            <span><span style="color: red">*</span> - поля, обязательные для заполнения</span>
                            <p>Город</p>
                            <div class="form-group">
                                <select class="select2" name="city">
                                    @foreach($city as $v)
                                        <option value="{{ $v['name'] }}">{{ $v['little_description'] }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <p>Район</p>
                            <div class="form-group"><select class="select2" name="area"></select></div>

                            <p>Адрес доставки</p>
                            <form role="form">
                                <div class="form-group">
                                    <input value="{{ $address }}" id="address" name="address" type="text" class="form-control class_for_input" placeholder="Город, улица, номер дома*" />
                                </div>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" name="private_house" class="flat" id="private_house"/>
                                        <input type="hidden" name="private_house_h" />
                                        Это частный дом
                                    </label>
                                </div>
                                <div class="form-group">
                                    <input name="apartment" id="apartment" type="text" class="form-control class_for_input" placeholder="Квартира или офис*" />
                                </div>
                                <div class="form-group">
                                    <input name="entrance_floor" id="entrance_floor" type="text" class="form-control class_for_input" placeholder="Подъезд/этаж"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <form role="form">
                        <div class="form-group">
                            <textarea class="form-control class_for_input" name="comment_1" id="comment_1" rows="8" placeholder="Комментарий для курьера,чтобы он быстро Вас нашел. Особенности подъезда, домофон,кодовый замок... Все что может помочь курьеру в поиске Вашего дома и квартиры."></textarea>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div class="col-md-12">
                    <div class="delivery_time">
                        <?php $t = explode('-', $time_current) ?>
                        <p>Время и дата доставки:</p>
                        <span>
                             <span class="time-current-ordering">{!! count($t) == 3 ?
                                (int) $time_current_day . ' '. $time_current_month . ', ' .
                                'с ' . $t[0] . ' до ' . $t[1] : 'время не выбрано'
                             !!}</span>
                            <a href="javascript:void(0)" data-toggle="modal" data-target=".timeModal">Сменить время </a>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div class="col-md-5">
                    <div class="delivery_address">
                        <p>Информация о получателе:</p>
                        <span> * - поля, обязательные для заполнения</span>
                        <form role="form">
                            <div class="form-group">
                                <input name="full_name" id="full_name" type="text" class="form-control class_for_input" placeholder="Фамилия Имя Отчество *"/>
                            </div>

                            <div class="form-group">
                                <input name="phone" id="phone" type="text" class="form-control class_for_input" placeholder="Номер телефона*"/>
                            </div>

                            <div class="form-group">
                                <input name="email" id="email" type="text" class="form-control class_for_input" placeholder="Адрес электронной почты *"/>
                            </div>

                            <div class="form-group">
                                <select class="select2" name="payment_method" id="payment_method">
                                    <option value="">Способ оплаты</option>
                                    <option value="cash">Наличный расчёт</option>
                                    <option value="noncash">Безналичный расчёт</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-7">
                    <form role="form">
                        <div class="form-group">
                            <textarea class="form-control class_for_input " id="comment_2" rows="8" name="comment_2" placeholder="Дополнительный комментарий к заказу: например, чтобы курьер не звонил в дверь и не разбудил детей"></textarea>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <hr />
                </div>
                <div class="col-md-12">
                    <div class="mess-ordering"></div>

                    <div class="col-md-7">
                        <div class="totals">
                            <strong>Итого:</strong>
                            <p>
                                Сумма заказа:

                                <strong>
                                    <span class="moneyTop"></span>
                                    <i class="glyphicon glyphicon-ruble"></i>
                                </strong>
                            </p>
                            <p>Доставка: <strong><span class="delivery-span">0</span> <i class="glyphicon glyphicon-ruble"></i></strong></p>
                            <input name="delivery" type="hidden" value="0"/>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="to_pay">
                            <div class="col-md-7">
                                <p class="text-right">
                                    К оплате:

                                    <span>
                                        <span class="moneyAll"></span>
                                        <i class="glyphicon glyphicon-ruble"></i>
                                    </span>
                                </p>
                            </div>
                            <div class="col-md-5">
                                <button type="button" onclick="filCat.send_app()" class="btn btn-danger text-right">
                                    Оформить заказ
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content -->

@section('footer')
    <script>
		var
			area = JSON.parse('<?= json_encode($area) ?>');

		function renderArea() {
			var
				t = '',
				r = $('[name="city"]').val();

			for(var i = 0; area.length > i; i++)
				if(r === area[i].parent_id)
					t += '<option value="' + area[i].price_money + '">' +
						area[i].little_description +
						'</option>';

			$('[name="area"]').html(t);
			changeArea();
		}

		function changeArea() {
			var
				val = $('[name="area"]').val();

			$('[name="delivery"]').val(val);
			$('.delivery-span').html(val);
			$('.moneyAll').html(+val + parseInt($('#moneyTopId').html()));

			if(!$('#moneyTopId').html())
				setTimeout(function () { changeArea(); }, 30);
		}

		// first load
		renderArea();

		// watch change
		$('[name="city"]').select2().on('change', function() { renderArea(); });
		$('[name="area"]').select2().on('change', function() { changeArea(); });
    </script>
@endsection
@stop
