@extends('shop.layouts.default')
@section('content')
    <!-- content -->
    <div class="container-fluid fon_wood">
        <div class="container">
            <div class="row breadmargin">
                <ol class="breadcrumb">
                    <li><a href="/shop">Главная</a></li>
                    <li><a href="#">Акции</a></li>
                </ol>
            </div>
            <div class="aboutcompany">
                <h2>Акции</h2>
            </div>
            <div class="cont">
                @foreach($actions as $val)
                    <div class="col-md-6">
                        <div class="stocks thumbnail">
                            <a href="/shop/actions/{{ $val['id'] }}">
                                @if($val['file'])
                                    @if($val['crop'])
                                        <img src="/images/files/small/{{ $val['crop'] }}" style="border-radius: 15px; overflow: hidden" />
                                    @else
                                        <img src="/images/files/small/{{ $val['file'] }}" style="border-radius: 15px; overflow: hidden" />
                                    @endif

                                @else
                                    <img src="/images/files/no-image.jpg" style="border-radius: 15px; overflow: hidden" />
                                @endif
                            </a>
                        </div>
                    </div>
                @endforeach

                {!! $actions->links() !!}
            </div>
        </div>
    </div>
    <!-- content -->
@stop
