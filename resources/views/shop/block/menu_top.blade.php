<!-- top menu -->
<div class="container-fluid">
    <div class="row">
        <nav class="navbar navbar-default navbar-static-top navbar_fon">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        @foreach($menu[0] as $val)
                            <li><a href="{{ $val['translation'] }}">{{ $val['name'] }}</a></li>
                        @endforeach
                    </ul>
                    <ul class="nav navbar-nav red-menu-top">
                        @foreach($menu[15] as $val)
                            <li><a href="/shop/categories/{{ $val['id'] }}"><div>{{ $val['name'] }}</div></a></li>
                        @endforeach
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><i class=" glyphicon glyphicon-log-in small-icon"></i> Вход</a></li>
                        <li><a href="#"><i class=" glyphicon glyphicon-user small-icon"></i> Регистрация</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
    </div>
</div>
<!-- top menu -->