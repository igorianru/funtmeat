<!-- menu header -->
<div class="container-fluid red_fon">
    <div class="row">
        <div class="container">
            <div class="masthead">
                <nav>
                    <ul class="nav nav-justified red-menu">
                        @foreach($menu[15] as $val)
                            <li><a href="/shop/categories/{{ $val['id'] }}"><div>{{ $val['name'] }}</div></a></li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- menu header -->