<!-- header -->
<div class="container-fluid white-container header">
    <div class="row">
        <div class="container" style="margin-bottom: 15px">
            <div class="col-md-2 col-xs-12 logo">
                <a href="/shop">
                    <img src="/images/shop/logo.png">
                </a>
            </div>
            <div class="col-md-4">
                <form class="navbar-form navbar-left search-top" role="search" method="get">
                    <div class="input-group">
                        <input type="text" name="s" class="form-control" placeholder="Поиск">

                        <span>
                            <button class="btn btn-default" type="button">
                                <i class="glyphicon glyphicon-search search"> </i>
                            </button>
                        </span>
                    </div><!-- /input-group -->
                </form>

            </div>

            <div class="col-md-3"><ul class="list-unstyled">{!! $params['header_address']['key'] !!}</ul></div>

            <div class="col-md-3 list-inline">
                <div class="row">
                    <!-- dropdown basket button -->
                    <a class="dropdown-toggle a color_of_a" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="col-md-6 shop-cart-div">
                            <p class="money1">Сумма:</p>
                            <p class="money">
                                <span class="moneyTop">0</span>
                                <i class="glyphicon glyphicon-ruble"></i>
                            </p>
                        </div>
                        <div class="col-md-6 shop-cart-div">
                            <div class="shop-cart">
                                <div class="c"></div>
                                <div class="p hidden">0</div>
                            </div>
                        </div>
                    </a>
                    <!-- dropdown basket button -->

                    <!-- dropdown basket body -->
                    <div class="dropdown-menu basket_dropdown ">
                        <div class="angle"></div>
                        <p>Корзина</p>
                        <div class="basketCont" id="basketCont">
                            <p>Корзина пуста</p>
                            <img src="/images/shop/logo.png" class="bgCategories">
                        </div>

                        <div class="col-md-12 a hidden">
                            <div class="basket_dropdown_btn">
                                <div class="col-md-6 b">
                                    <p>Итог:</p>
                                    <span>490<i class=" glyphicon glyphicon-ruble"></i></span>
                                </div>
                                <div class="col-md-6 c text-right">
                                    <div>
                                        <button type="button" class="btn btn-danger" onclick="href('/shop/basket')">
                                            Перейти в корзину
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- dropdown basket body -->
                </div>
            </div>

            <a
                class="btn btn-link btn-link-time hidden"
                href="javascript:void(0)"
                data-toggle="modal"
                 data-target=".timeModal"
            >
                Выбрать время доставки
            </a>
        </div>
    </div>
</div>
<!-- header -->