<!-- Modal -->
<div class="modal fade modalk" id="callback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Обратный звонок</h4>
            </div>
            <div class="modal-body text-center review-form">
                <p>
                    Закажите обратный звонок
                    - мы перезвоним Вам!
                </p>
                <br class="clear"/>
                <div class="form-group">
                    <input type="text" class="form-control name_min" name="name" placeholder="Имя"/>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control phone_min" name="phone" placeholder="Телефон"/>
                </div>
                <br class="clear"/>
                <div class="form-group">
                    <button type="button" class="btn btn-main color-red" onclick="send_mail('min')">заказать обратный звонок</button>
                </div>

                <div class="mess_min"></div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade timeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Время и район доставки</h4>
            </div>

            <div class="modal-body text-center table-time">
                <div class="area">
                    <div class="title">Адрес доставки</div>
                    <div>
                        <input
                            placeholder="Адрес доставки"
                            class="form-control delivery-address-top"
                            value="{{ $address }}"
                        />
                    </div>
                </div>


                <div class="col-md-2 col-14">
                    <div class="header">Сегодня</div>
                    <div class="day"></div>

                    <div>
                        @foreach($range_time as $val)
                            <div
                                class="select-time {!! !$val['active'] ? 'disabled' : '' !!} {!! $time_current == $val['start'] . '-' . $val['end'] . '-' . Date('w')  ? 'active' : '' !!}"
                                data-time="{{ $val['start'] . '-'. $val['end'] . '-' . Date('d') }}"
                                data-month="{{ \App\Classes\Base::$months[Date('m')] }}"
                                data-day="{{ Date('d') }}"
                            >
                                {{ $val['start'] }} - {{ $val['end'] }}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-2 col-14">
                    <div class="header">Завтра</div>
                    <div class="day"></div>

                    <div>
                        @foreach($range_time as $val)
                            <div
                                class="select-time {!! $time_current == $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+1 day")) ? 'active' : '' !!}"
                                data-time="{{ $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+1 day")) }}"
                                data-month="{{ \App\Classes\Base::$months[Date('m', strtotime("+1 day"))] }}"
                                data-day="{{ Date('d', strtotime("+1 day")) }}"
                            >
                                {{ $val['start'] }} - {{ $val['end'] }}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-2 col-14">
                    <div class="header">{{ \App\Classes\Base::$day_of_week[Date('w', strtotime("+2 day"))] }}</div>

                    <div class="day">
                        {{ (int) Date('d', strtotime("+2 day")) }}
                        {{ \App\Classes\Base::$months[Date('m', strtotime("+2 day"))] }}
                    </div>

                    <div>
                        @foreach($range_time as $val)
                            <div
                                class="select-time {!! $time_current == $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+2 day")) ? 'active' : '' !!}"
                                data-time="{{ $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+2 day")) }}"
                                data-month="{{ \App\Classes\Base::$months[Date('m', strtotime("+2 day"))] }}"
                                data-day="{{ Date('d', strtotime("+2 day")) }}"
                            >
                                {{ $val['start'] }} - {{ $val['end'] }}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-2 col-14">
                    <div class="header">{{ \App\Classes\Base::$day_of_week[Date('w', strtotime("+3 day"))] }}</div>

                    <div class="day">
                        {{ (int) Date('d', strtotime("+3 day")) }}
                        {{ \App\Classes\Base::$months[Date('m', strtotime("+3 day"))] }}
                    </div>

                    <div>
                        @foreach($range_time as $val)
                            <div
                                class="select-time {!! $time_current == $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+3 day")) ? 'active' : '' !!}"
                                data-time="{{ $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+3 day")) }}"
                                data-month="{{ \App\Classes\Base::$months[Date('m', strtotime("+3 day"))] }}"
                                data-day="{{ Date('d', strtotime("+3 day")) }}"
                            >
                                {{ $val['start'] }} - {{ $val['end'] }}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-2 col-14">
                    <div class="header">{{ \App\Classes\Base::$day_of_week[Date('w', strtotime("+4 day"))] }}</div>

                    <div class="day">
                        {{ (int) Date('d', strtotime("+4 day")) }}
                        {{ \App\Classes\Base::$months[Date('m', strtotime("+4 day"))] }}
                    </div>

                    <div>
                        @foreach($range_time as $val)
                            <div
                                class="select-time {!! $time_current == $val['start'] . '-' . $val['end'] ? 'active' . '-' . Date('d', strtotime("+4 day")) : '' !!}"
                                data-time="{{ $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+4 day")) }}"
                                data-month="{{ \App\Classes\Base::$months[Date('m', strtotime("+4 day"))] }}"
                                data-day="{{ Date('d', strtotime("+4 day")) }}"
                            >
                                {{ $val['start'] }} - {{ $val['end'] }}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-2 col-14">
                    <div class="header">{{ \App\Classes\Base::$day_of_week[Date('w', strtotime("+5 day"))] }}</div>

                    <div class="day">
                        {{ (int) Date('d', strtotime("+5 day")) }}
                        {{ \App\Classes\Base::$months[Date('m', strtotime("+5 day"))] }}
                    </div>

                    <div>
                        @foreach($range_time as $val)
                            <div
                                class="select-time {!! $time_current == $val['start'] . '-' . $val['end'] ? 'active' . '-' . Date('d', strtotime("+5 day")) : '' !!}"
                                data-time="{{ $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+5 day")) }}"
                                data-month="{{ \App\Classes\Base::$months[Date('m', strtotime("+5 day"))] }}"
                                data-day="{{ Date('d', strtotime("+5 day")) }}"
                            >
                                {{ $val['start'] }} - {{ $val['end'] }}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-2 col-14">
                    <div class="header">{{ \App\Classes\Base::$day_of_week[Date('w', strtotime("+6 day"))] }}</div>

                    <div class="day">
                          {{(int) Date('d', strtotime("+6 day")) }}
                        {{ \App\Classes\Base::$months[Date('m', strtotime("+6 day"))] }}
                    </div>

                    <div>
                        @foreach($range_time as $val)
                            <div
                                class="select-time {!! $time_current == $val['start'] . '-' . $val['end'] ? 'active' . '-' . Date('d', strtotime("+6 day")) : '' !!}"
                                data-time="{{ $val['start'] . '-' . $val['end'] . '-' . Date('d', strtotime("+6 day")) }}"
                                data-month="{{ \App\Classes\Base::$months[Date('m', strtotime("+6 day"))] }}"
                                data-day="{{ Date('d', strtotime("+6 day")) }}"
                            >
                                {{ $val['start'] }} - {{ $val['end'] }}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="text-center">
                <input type="hidden" class="time-value-current" value="{{ $time_current }}" />
                <input type="hidden" class="time-value-current-month" value="{{ $time_current_month }}" />
                <input type="hidden" class="time-value-current-day" value="{{ $time_current_day }}" />
                <button class="btn btn-danger" onclick="filCat.selectTime()">Выбрать</button>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!-- footer -->
<div class="container-fluid fon_footer">
    <div class="row">
        <div class="container w ">
            <div class="row">
                <div class="col-md-5">
                    <div class="row">
                        <ul class="footer-menu">
                            @foreach($menu[0] as $val)
                                <li>
                                    <a href="{{ $val['translation'] }}" title="{{ $val['name'] }}">
                                        {{ $val['name'] }}
                                    </a>
                                </li>
                            @endforeach
                            @foreach($menu[1] as $val)
                                <li>
                                    <a href="{{ $val['translation'] }}" title="{{ $val['name'] }}">
                                        {{ $val['name'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-icon">
                        <div class="vk"></div>
                        <div class="fb"></div>
                        <div class="in"></div>
                    </div>
                </div>

                <div class="col-md-4 text-right">{!! $params['footer_address']['key'] !!}</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid fon_footer footer-border">
    <div class="container">
        <div class="footer-text"> ООО«Фунт Мяса» C 2016 - {{ Date('Y') }} Все права защищены</div>
    </div>
</div>
<!-- footer -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('/js/shop/bootstrap.min.js') }}"></script>
<!-- select2 -->
<script src="{{ asset('/js/shop/select2/select2.full.min.js') }}"></script>
<!-- lodash -->
<script src="/js/lodash.min.js"></script>
<!-- Switchery -->
<script src="{{ asset('/modules/js/switchery/switchery.min.js') }}"></script>
<!-- filters-categories -->
<script src="{{ asset('/js/shop/filters-categories.js') }}"></script>
<!-- icheck -->
<script src="{{ asset('/modules/js/icheck/icheck.min.js') }}"></script>
<!-- constants -->
<script src="/js/shop/constants.js"></script>
<!-- validators -->
<script src="/js/shop/validators.js"></script>
@if(!$__env->yieldContent('footer_cart'))
    <script>
		$(document).ready(function(){
			filCat.initialize({
				cont      : '.selResult',
				isLoadCat : false,
				num       : '.selReN > .i',
				pagination: true,
				url_req   : '/',
			});

			setTimeout(function () { filCat.selectCatalogs(); }, 500);
		});
    </script>

@endif
@yield('footer_cart')
@yield('footer')
<!-- custom -->
<script src="{{ asset('/js/custom.js') }}"></script>

<script>
	function err_inp (cl) {
		$(cl).css({boxShadow: "0px 0px 3px 2px rgb(183, 42, 31)"});
		setTimeout('$("'+cl+'").css({boxShadow: "none"})', 2500);
	}

	function send_mail(type) {
		var dataString;
		var name = $(".name_"+type).val();
		var phone = $(".phone_"+type).val();

		if(name.trim() == '') {
			err_inp(".name_"+type+"");
			return false
		}

		if(type == 'max') {
			var text = $(".text_"+type).val();

			dataString = 'type=' + type + '&name=' + name + '&phone=' + phone + '&text=' + text;
		} else {
			dataString = 'type=' + type + '&name=' + name + '&phone=' + phone + '&text=' + '';
		}

		if(phone.trim() == '') {
			err_inp(".phone_"+type+"");

			return false
		}

		$.ajax({
			cache: false,
			url: '/send_mail',
			data: dataString,
			type: 'post',
			dataType: 'JSON',
			success: function (data) {
				if (data['result'] == 'ok') {
					$('.mess_' + type).html('<div style="margin-top: 10px" class="alert alert-success">Запрос успешно отправлен</div>').show(300);

					setTimeout(function () {
						$(".mess_" + type).hide(300);
						$("#callback").modal("hide");
					}, 2000);

					$(".name_"+type).val('');
					$(".phone_"+type).val('');

					if(type == 'max') {
						$(".email_"+type).val('');
					}
				}
			}
		});
	}
</script>
</body>
</html>