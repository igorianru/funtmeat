<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="@if (isset($meta['title'])) {{ $meta['title'] }} @endif" />
    <meta content="@if (isset($meta['description'])) {{ $meta['description'] }} @endif" name="description">
    <meta content="@if (isset($meta['keywords'])) {{ $meta['keywords'] }} @endif" name="keywords">
    <meta http-equiv="Last-Modified" content="{{ !empty($lastModified) ? $lastModified:'Fri, 11 Sep 2015 11:20:27' }}">
    <link rel="icon" type="image/x-icon" href="/images/site/logoF.png">
    <title>@if (isset($meta['title'])) {{ $meta['title'] }} @endif</title>

    <!-- Bootstrap -->
    <link href="{{ asset('/css/shop/bootstrap.min.css') }}" rel="stylesheet">
    <!-- select2 -->
    <link href="{{ asset('/css/shop/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/shop/style.css') }}" rel="stylesheet">
    <!-- icheck -->
    <link href="{{ asset('/modules/css/icheck/flat/red.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('header')
</head>

<body>
@include('shop.block.menu_top')
@include('shop.block.header')
@include('shop.block.menu_header')
<?php
if (!empty($message)){
    if (key($message)=="error"){
        ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ $message["error"] }}
        </div>
        <?php
    }
    if (key($message)=="info"){
        ?>
        <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ $message["info"] }}
        </div>
        <?php
    }
}
?>
@yield('content')
@include('shop.layouts.footer')